-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 30, 2017 at 08:23 PM
-- Server version: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lukbar4`
--

-- --------------------------------------------------------

--
-- Table structure for table `aptarnauja`
--

CREATE TABLE `aptarnauja` (
  `fk_SANDELISid` int(11) NOT NULL DEFAULT '0',
  `fk_DARBUOTOJAStabelio_nr` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci;

--
-- Dumping data for table `aptarnauja`
--

INSERT INTO `aptarnauja` (`fk_SANDELISid`, `fk_DARBUOTOJAStabelio_nr`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 30),
(1, 31),
(1, 32),
(1, 33),
(1, 60),
(1, 61),
(1, 62),
(1, 63),
(2, 4),
(2, 5),
(2, 6),
(2, 34),
(2, 35),
(2, 36),
(2, 64),
(2, 65),
(2, 66),
(3, 7),
(3, 8),
(3, 9),
(3, 37),
(3, 38),
(3, 39),
(3, 67),
(3, 68),
(3, 69),
(4, 10),
(4, 11),
(4, 12),
(4, 40),
(4, 41),
(4, 42),
(4, 70),
(4, 71),
(4, 72),
(5, 13),
(5, 14),
(5, 15),
(5, 43),
(5, 44),
(5, 45),
(5, 73),
(5, 74),
(5, 75),
(6, 16),
(6, 17),
(6, 18),
(6, 46),
(6, 47),
(6, 48),
(6, 76),
(6, 77),
(6, 78),
(7, 19),
(7, 20),
(7, 21),
(7, 49),
(7, 50),
(7, 51),
(7, 79),
(7, 80),
(8, 22),
(8, 23),
(8, 24),
(8, 25),
(8, 52),
(8, 53),
(8, 54),
(8, 55),
(9, 26),
(9, 27),
(9, 56),
(9, 57),
(10, 28),
(10, 29),
(10, 58),
(10, 59);

-- --------------------------------------------------------

--
-- Table structure for table `darbuotojas`
--

CREATE TABLE `darbuotojas` (
  `vardas` varchar(255) COLLATE utf8_lithuanian_ci DEFAULT NULL,
  `pavarde` varchar(255) COLLATE utf8_lithuanian_ci DEFAULT NULL,
  `tabelio_nr` int(11) NOT NULL DEFAULT '0',
  `fk_NUOMOS_BIURASid` int(11) NOT NULL,
  `fk_NAUDOTOJASid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci;

--
-- Dumping data for table `darbuotojas`
--

INSERT INTO `darbuotojas` (`vardas`, `pavarde`, `tabelio_nr`, `fk_NUOMOS_BIURASid`, `fk_NAUDOTOJASid`) VALUES
('Jonax', 'Jonaitis', 1, 1, 1),
('Mariusx', 'Marijonaitisx', 2, 1, 2),
('Paulius', 'Pauliukevicius', 3, 1, 3),
('Lukas', 'Lukauskis', 4, 1, 4),
('Rokas', 'Rokauskis', 5, 1, 5),
('Saulius', 'Sauliuskevicius', 6, 1, 6),
('Martynas', 'Martinkonas', 7, 2, 7),
('Martis', 'Butautas', 8, 2, 8),
('Marcius', 'Radzevicius', 9, 2, 9),
('Juozapas', 'Dainiupras', 10, 2, 10),
('Juozas', 'Dainiuskevicius', 11, 2, 11),
('Arnas', 'Milaknis', 12, 2, 12),
('Arnoldas', 'Jasikevicius', 13, 3, 13),
('Steponas', 'Marciulionis', 14, 3, 14),
('Stepas', 'Garastas', 15, 3, 15),
('Povilas', 'Bakchis', 16, 3, 16),
('Ilona', 'Valaityte', 17, 3, 17),
('Ruta', 'Kutka', 18, 3, 18),
('Rasa', 'Draugelaite', 19, 4, 19),
('Giedrius', 'Brilius', 20, 4, 20),
('Ernestas', 'Anuprevicius', 21, 4, 21),
('Ernvaldas', 'Dainiauskas', 22, 4, 22),
('Dainius', 'Virsiliskis', 23, 4, 23),
('Dainora', 'Dainoriene', 24, 4, 24),
('Jonanas', 'Valaitis', 25, 5, 25),
('Dijora', 'Joskiene', 26, 5, 26),
('Darijonas', 'Juskevicius', 27, 5, 27),
('Faustas', 'Janavicius', 28, 5, 28),
('Faustina', 'Kutkaite', 29, 5, 29),
('Giedre', 'Jurgeleviciute', 30, 5, 30),
('Žadvydė', 'Talius', 31, 1, 31),
('Žakas', 'Tamara', 32, 1, 32),
('Žaklina', 'Tania', 33, 1, 33),
('Žana', 'Tanvilas', 34, 1, 34),
('Žanas', 'Tanvilė', 35, 1, 35),
('Žaneta', 'Tarena', 36, 1, 36),
('Žavinta', 'Tarenas', 37, 2, 37),
('Žeimantas', 'Tarmantas', 38, 2, 38),
('Žeimantė', 'Tarmantė', 39, 2, 39),
('Žemyna', 'Tarvainas', 40, 2, 40),
('Ženevjeva', 'Tarvainė', 41, 2, 41),
('Žeraldas', 'Tarvilas', 42, 2, 42),
('Žermena', 'Tarvilė', 43, 3, 43),
('Žėrutė', 'Tarvina', 44, 3, 44),
('Žėrutis', 'Tarvinas', 45, 3, 45),
('Žybartas', 'Tatjana', 46, 3, 46),
('Žybartė', 'Tauras', 47, 3, 47),
('Žibuoklė', 'Taurė', 48, 3, 48),
('Žibutė', 'Taurija', 49, 4, 49),
('Žydrė', 'Taurijus', 50, 4, 50),
('Žydrius', 'Taurys', 51, 4, 51),
('Žydrūnas', 'Taurius', 52, 4, 52),
('Žydrūnė', 'Tautas', 53, 4, 53),
('Žydrutė', 'Tautė', 54, 4, 54),
('Žydrutis', 'Tautenė', 55, 5, 55),
('Žiedė', 'Tautenis', 56, 5, 56),
('Žygaudas', 'Tautgailas', 57, 5, 57),
('Žygaudė', 'Tautgailė', 58, 5, 58),
('Žygimantas', 'Tautgina', 59, 5, 59),
('Žygimantė', 'Tautginas', 60, 5, 60),
('Žyginta', 'Tautginė', 61, 1, 61),
('Žygintas', 'Tautginta', 62, 1, 62),
('Žygintė', 'Tautgintas', 63, 5, 63),
('Žygis', 'Tautgintė', 64, 1, 64),
('Žygmantas', 'Tautgirdas', 65, 5, 65),
('Žygmantė', 'Tautgirdė', 66, 1, 66),
('Žygūnas', 'Tautkanta', 67, 2, 67),
('Žilvija', 'Tautkantas', 68, 2, 68),
('Žilvijus', 'Tautmilas', 69, 2, 69),
('Žilvinas', 'Tautmilė', 70, 2, 70),
('Žymantas', 'Raiskas', 71, 2, 71),
('Žymantė', 'Tautrimė', 72, 2, 72),
('Žintautas', 'Tautvaldas', 73, 3, 73),
('Žintautė', 'Tautvaldė', 74, 3, 74),
('Žyvilas', 'Tautvė', 75, 3, 75),
('Živilė', 'Tautvyda', 76, 3, 76),
('Žyvilė', 'Tautvydas', 77, 3, 77),
('Živilis', 'Tautvydė', 78, 3, 78),
('Žvainibudas', 'Tautvilas', 79, 4, 79),
('Aurelijus', 'Veryga', 80, 1, 80),
('Rokas', 'Raiskas', 81, 2, 81),
('Arnas', 'Ambotas', 82, 1, 82);

-- --------------------------------------------------------

--
-- Table structure for table `firma`
--

CREATE TABLE `firma` (
  `pavadinimas` varchar(255) COLLATE utf8_lithuanian_ci DEFAULT NULL,
  `id` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci;

--
-- Dumping data for table `firma`
--

INSERT INTO `firma` (`pavadinimas`, `id`) VALUES
('Makita', 1),
('Dewalt', 2),
('Milwauke', 3),
('Bosch', 4),
('Metabo', 5),
('Skill', 6),
('Wagner', 7),
('Duracel', 8),
('Aeton', 9),
('FestTOOLS', 10),
('Union', 11),
('Gaul', 12),
('Mitsubishi', 13),
('JapanTools', 14),
('MicroStar', 15),
('ProEquipment', 16),
('NGK', 17),
('Veilside', 18),
('Daradine', 19),
('Ecoline', 20),
('Nukem', 21),
('Duke', 22);

-- --------------------------------------------------------

--
-- Table structure for table `irankis`
--

CREATE TABLE `irankis` (
  `id` int(11) NOT NULL DEFAULT '0',
  `pagaminimo_data` date DEFAULT NULL,
  `greicio_tipai` varchar(255) COLLATE utf8_lithuanian_ci DEFAULT NULL,
  `kuro_tipas` varchar(255) COLLATE utf8_lithuanian_ci DEFAULT NULL,
  `irankio_tipas` varchar(255) COLLATE utf8_lithuanian_ci DEFAULT NULL,
  `galingumas` int(11) DEFAULT NULL,
  `dydis` varchar(255) COLLATE utf8_lithuanian_ci DEFAULT NULL,
  `busena` varchar(255) COLLATE utf8_lithuanian_ci DEFAULT NULL,
  `svoris` double DEFAULT NULL,
  `fk_MODELISid` int(11) NOT NULL,
  `fk_IRANKIU_GRUPEid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci;

--
-- Dumping data for table `irankis`
--

INSERT INTO `irankis` (`id`, `pagaminimo_data`, `greicio_tipai`, `kuro_tipas`, `irankio_tipas`, `galingumas`, `dydis`, `busena`, `svoris`, `fk_MODELISid`, `fk_IRANKIU_GRUPEid`) VALUES
(0, '2016-01-29', 'reguliuojamas', 'benzinas', 'suktuvas', 1, 'miniatiurinis', 'laisvas', 1, 4, 2),
(1, '2013-06-04', 'letaeigis', 'dyzelinas', 'greztuvas', 1387, 'miniatiūrinis', 'laisvas', 4681, 1, 1),
(2, '2015-06-27', 'greitaeigis', 'benzinas/dujos', 'spaustuvas', 2146, 'kompaktiškas', 'laisvas', 61, 2, 3),
(3, '2014-08-04', 'reguliuojamas', 'dujos', 'oblius', 2017, 'miniatiūrinis', 'isnuomotas', 9187, 3, 2),
(4, '2013-03-26', 'greitaeigis', 'benzinas', 'kaltuvas', 7714, 'vidutinis', 'laisvas', 330, 4, 2),
(5, '2011-11-02', 'letaeigis', 'dujos', 'spaustuvas', 6474, 'miniatiūrinis', 'isnuomotas', 5544, 5, 1),
(6, '2016-04-11', 'greitaeigis', 'dyzelinas', 'suktuvas', 8805, 'vidutinis', 'isnuomotas', 6369, 6, 1),
(7, '2013-12-18', 'letaeigis', 'benzinas', 'kaltuvas', 3488, 'miniatiūrinis', 'laisvas', 327, 7, 3),
(8, '2010-06-19', 'greitaeigis', 'benzinas/dujos', 'greztuvas', 661, 'vidutinis', 'remontuojamas', 1894, 8, 1),
(9, '2012-01-11', 'reguliuojamas', 'benzinas', 'oblius', 2599, 'didelis', 'laisvas', 6440, 9, 4),
(10, '2014-02-16', 'greitaeigis', 'dujos', 'suktuvas', 9014, 'miniatiūrinis', 'isnuomotas', 1175, 10, 3),
(11, '2016-02-22', 'greitaeigis', 'benzinas', 'spaustuvas', 3702, 'vidutinis', 'laisvas', 8749, 11, 2),
(12, '2015-08-24', 'letaeigis', 'benzinas/dujos', 'siaurapjuklis', 7842, 'didelis', 'isnuomotas', 8158, 12, 2),
(13, '2010-02-27', 'greitaeigis', 'dyzelinas', 'kaltuvas', 2866, 'kompaktiškas', 'isnuomotas', 222, 13, 4),
(14, '2015-07-23', 'greitaeigis', 'dujos', 'oblius', 9064, 'vidutinis', 'remontuojamas', 4042, 14, 3),
(15, '2010-01-25', 'reguliuojamas', 'benzinas/dujos', 'spaustuvas', 5489, 'labai didelis', 'laisvas', 8813, 15, 1),
(16, '2013-11-06', 'letaeigis', 'elektra', 'siaurapjuklis', 1018, 'didelis', 'tikrinamas', 8699, 16, 3),
(17, '2012-10-16', 'greitaeigis', 'dyzelinas', 'greztuvas', 6334, 'miniatiūrinis', 'isnuomotas', 9043, 17, 3),
(18, '2011-04-02', 'greitaeigis', 'elektra', 'kaltuvas', 671, 'vidutinis', 'remontuojamas', 9953, 18, 3),
(19, '2010-01-17', 'greitaeigis', 'elektra', 'spaustuvas', 9742, 'labai didelis', 'laisvas', 278, 19, 1),
(20, '2016-02-26', 'letaeigis', 'dyzelinas', 'oblius', 9075, 'didelis', 'isnuomotas', 3230, 20, 1),
(21, '2012-07-07', 'greitaeigis', 'elektra', 'kampinis slifuoklis', 2162, 'kompaktiškas', 'tikrinamas', 5116, 21, 3),
(22, '2014-05-23', 'letaeigis', 'dujos', 'spaustuvas', 9827, 'labai didelis', 'laisvas', 7190, 22, 4),
(23, '2014-04-19', 'greitaeigis', 'dyzelinas', 'kaltuvas', 7861, 'vidutinis', 'tikrinamas', 4926, 23, 3),
(24, '2016-05-12', 'reguliuojamas', 'elektra', 'greztuvas', 8202, 'didelis', 'laisvas', 4756, 24, 2),
(25, '2014-12-19', 'greitaeigis', 'benzinas/dujos', 'kampinis slifuoklis', 8231, 'didelis', 'isnuomotas', 8029, 25, 2),
(26, '2016-01-23', 'letaeigis', 'elektra', 'kaltuvas', 5040, 'miniatiūrinis', 'laisvas', 9512, 26, 3),
(27, '2011-02-05', 'greitaeigis', 'elektra', 'spaustuvas', 7360, 'kompaktiškas', 'tikrinamas', 1980, 27, 1),
(28, '2015-03-18', 'greitaeigis', 'benzinas/dujos', 'siaurapjuklis', 2612, 'didelis', 'laisvas', 3941, 28, 2),
(29, '2016-12-25', 'greitaeigis', 'elektra', 'greztuvas', 8569, 'didelis', 'tikrinamas', 2304, 29, 4),
(30, '2013-09-07', 'reguliuojamas', 'benzinas', 'poliruoklis', 6973, 'didelis', 'isnuomotas', 8822, 30, 3),
(31, '2016-04-22', 'letaeigis', 'elektra', 'spaustuvas', 486, 'labai didelis', 'laisvas', 3033, 31, 1),
(32, '2013-09-22', 'greitaeigis', 'elektra', 'siaurapjuklis', 9620, 'vidutinis', 'isnuomotas', 5727, 32, 2),
(33, '2016-02-06', 'greitaeigis', 'elektra', 'suktuvas', 7724, 'labai didelis', 'isnuomotas', 9206, 33, 4),
(34, '2013-02-27', 'letaeigis', 'elektra', 'greztuvas', 5843, 'kompaktiškas', 'isnuomotas', 5762, 34, 2),
(35, '2013-01-23', 'greitaeigis', 'elektra', 'poliruoklis', 9478, 'didelis', 'laisvas', 4110, 35, 2),
(36, '2016-11-06', 'greitaeigis', 'benzinas', 'oblius', 5264, 'didelis', 'isnuomotas', 494, 36, 3),
(37, '2016-08-28', 'reguliuojamas', 'elektra', 'spaustuvas', 1509, 'kompaktiškas', 'tikrinamas', 2016, 37, 3),
(38, '2012-11-28', 'greitaeigis', 'elektra', 'kaltuvas', 9752, 'didelis', 'laisvas', 1293, 38, 2),
(39, '2016-10-26', 'letaeigis', 'dyzelinas', 'suktuvas', 9195, 'didelis', 'laisvas', 3718, 39, 1),
(40, '2013-08-11', 'greitaeigis', 'elektra', 'oblius', 7578, 'didelis', 'isnuomotas', 8776, 40, 2),
(41, '2015-03-10', 'reguliuojamas', 'elektra', 'greztuvas', 8088, 'miniatiūrinis', 'laisvas', 7464, 41, 2),
(42, '2014-05-15', 'greitaeigis', 'elektra', 'poliruoklis', 257, 'vidutinis', 'remontuojamas', 2882, 42, 3),
(43, '2014-06-06', 'letaeigis', 'elektra', 'kaltuvas', 3349, 'labai didelis', 'isnuomotas', 811, 43, 2),
(44, '2013-05-04', 'greitaeigis', 'benzinas', 'oblius', 5056, 'didelis', 'isnuomotas', 2560, 44, 4),
(45, '2011-09-05', 'greitaeigis', 'elektra', 'greztuvas', 2144, 'didelis', 'laisvas', 6770, 45, 3),
(46, '2017-01-16', 'greitaeigis', 'elektra', 'greztuvas', 5122, 'didelis', 'tikrinamas', 150, 46, 3),
(47, '2014-05-14', 'greitaeigis', 'elektra', 'greztuvas', 2691, 'miniatiūrinis', 'isnuomotas', 9935, 47, 4),
(48, '2017-01-03', 'greitaeigis', 'elektra', 'kampinis slifuoklis', 2412, 'labai didelis', 'isnuomotas', 8489, 48, 4),
(49, '2013-01-11', 'reguliuojamas', 'elektra', 'oblius', 7700, 'kompaktiškas', 'laisvas', 8061, 49, 4),
(50, '2011-10-05', 'greitaeigis', 'dyzelinas', 'oblius', 818, 'labai didelis', 'tikrinamas', 588, 50, 3),
(51, '2012-05-14', 'letaeigis', 'elektra', 'siaurapjuklis', 8289, 'kompaktiškas', 'isnuomotas', 1341, 51, 3),
(52, '2015-10-22', 'greitaeigis', 'elektra', 'spaustuvas', 686, 'labai didelis', 'isnuomotas', 5174, 52, 3),
(53, '2015-03-27', 'greitaeigis', 'elektra', 'suktuvas', 2270, 'vidutinis', 'laisvas', 2631, 53, 1),
(54, '2013-11-13', 'reguliuojamas', 'benzinas', 'oblius', 5651, 'labai didelis', 'laisvas', 6089, 54, 2),
(55, '2015-04-19', 'letaeigis', 'elektra', 'poliruoklis', 4999, 'miniatiūrinis', 'isnuomotas', 1969, 55, 1),
(56, '2016-09-26', 'greitaeigis', 'elektra', 'kaltuvas', 1355, 'labai didelis', 'tikrinamas', 5565, 56, 2),
(57, '2011-10-16', 'greitaeigis', 'elektra', 'greztuvas', 1631, 'kompaktiškas', 'laisvas', 4914, 57, 2),
(58, '2016-12-18', 'reguliuojamas', 'elektra', 'greztuvas', 3208, 'didelis', 'tikrinamas', 1877, 58, 2),
(59, '2011-07-01', 'letaeigis', 'elektra', 'greztuvas', 716, 'didelis', 'laisvas', 8596, 59, 1),
(60, '2012-05-11', 'greitaeigis', 'benzinas', 'greztuvas', 5177, 'miniatiūrinis', 'laisvas', 351, 60, 2),
(61, '2010-05-08', 'greitaeigis', 'elektra', 'greztuvas', 3414, 'labai didelis', 'isnuomotas', 9888, 61, 1),
(62, '2010-09-08', 'reguliuojamas', 'dyzelinas', 'greztuvas', 7680, 'didelis', 'isnuomotas', 9517, 62, 3),
(63, '2016-01-03', 'letaeigis', 'elektra', 'suktuvas', 784, 'didelis', 'remontuojamas', 2260, 63, 2),
(64, '2010-08-27', 'greitaeigis', 'elektra', 'poliruoklis', 8967, 'didelis', 'isnuomotas', 8165, 64, 3),
(65, '2011-05-13', 'greitaeigis', 'elektra', 'poliruoklis', 623, 'vidutinis', 'laisvas', 1034, 65, 1),
(66, '2010-01-17', 'greitaeigis', 'elektra', 'kampinis slifuoklis', 3406, 'labai didelis', 'remontuojamas', 7079, 66, 2),
(67, '2015-04-17', 'letaeigis', 'benzinas', 'oblius', 4085, 'didelis', 'laisvas', 8368, 67, 4),
(68, '2015-04-19', 'greitaeigis', 'elektra', 'kampinis slifuoklis', 9485, 'didelis', 'isnuomotas', 385, 68, 4),
(69, '2015-06-18', 'greitaeigis', 'elektra', 'spaustuvas', 6077, 'didelis', 'tikrinamas', 4660, 69, 1),
(70, '2011-02-23', 'greitaeigis', 'dyzelinas', 'kaltuvas', 8334, 'labai didelis', 'laisvas', 301, 70, 4),
(71, '2016-02-23', 'letaeigis', 'elektra', 'poliruoklis', 3985, 'vidutinis', 'isnuomotas', 3622, 71, 3),
(72, '2010-12-25', 'greitaeigis', 'elektra', 'kampinis slifuoklis', 6115, 'labai didelis', 'tikrinamas', 8047, 72, 4),
(73, '2016-09-10', 'greitaeigis', 'elektra', 'oblius', 5467, 'labai didelis', 'laisvas', 2939, 73, 2),
(74, '2010-05-05', 'greitaeigis', 'benzinas', 'greztuvas', 5187, 'kompaktiškas', 'tikrinamas', 9871, 74, 3),
(75, '2013-01-06', 'reguliuojamas', 'elektra', 'greztuvas', 1510, 'vidutinis', 'laisvas', 4913, 75, 4),
(76, '2011-05-26', 'greitaeigis', 'elektra', 'greztuvas', 6101, 'didelis', 'laisvas', 3031, 76, 2),
(77, '2011-04-02', 'greitaeigis', 'elektra', 'oblius', 2729, 'didelis', 'isnuomotas', 7341, 77, 3),
(78, '2014-03-30', 'greitaeigis', 'elektra', 'greztuvas', 5813, 'didelis', 'laisvas', 3014, 78, 4),
(79, '2011-10-22', 'letaeigis', 'dyzelinas', 'greztuvas', 8670, 'kompaktiškas', 'remontuojamas', 6591, 79, 4),
(80, '2013-05-12', 'greitaeigis', 'elektra', 'spaustuvas', 1249, 'didelis', 'laisvas', 6529, 80, 2),
(81, '2011-07-22', 'greitaeigis', 'elektra', 'kaltuvas', 3642, 'didelis', 'isnuomotas', 7623, 81, 2),
(82, '2010-11-26', 'greitaeigis', 'elektra', 'poliruoklis', 8741, 'didelis', 'isnuomotas', 1763, 82, 1),
(83, '2013-12-30', 'greitaeigis', 'benzinas', 'greztuvas', 1909, 'miniatiūrinis', 'laisvas', 3507, 83, 4),
(84, '2015-01-18', 'greitaeigis', 'elektra', 'greztuvas', 1616, 'vidutinis', 'isnuomotas', 9968, 84, 3),
(85, '2011-09-07', 'greitaeigis', 'dyzelinas', 'poliruoklis', 9303, 'didelis', 'tikrinamas', 7215, 85, 3),
(86, '2010-07-27', 'letaeigis', 'elektra', 'spaustuvas', 6343, 'didelis', 'laisvas', 5359, 86, 3),
(87, '2016-09-10', 'greitaeigis', 'elektra', 'poliruoklis', 4209, 'didelis', 'remontuojamas', 1979, 87, 4),
(88, '2011-06-14', 'greitaeigis', 'elektra', 'poliruoklis', 6618, 'didelis', 'isnuomotas', 6630, 88, 1),
(89, '2011-07-15', 'greitaeigis', 'benzinas', 'oblius', 3600, 'didelis', 'isnuomotas', 9981, 89, 1),
(90, '2011-01-31', 'greitaeigis', 'elektra', 'poliruoklis', 7728, 'miniatiūrinis', 'remontuojamas', 5980, 90, 2),
(91, '2016-08-18', 'letaeigis', 'dyzelinas', 'poliruoklis', 5933, 'labai didelis', 'laisvas', 2467, 91, 4),
(92, '2010-07-10', 'greitaeigis', 'elektra', 'greztuvas', 9523, 'didelis', 'isnuomotas', 9029, 92, 3),
(93, '2012-05-20', 'greitaeigis', 'elektra', 'kaltuvas', 8686, 'didelis', 'tikrinamas', 6923, 93, 1),
(94, '2016-02-16', 'greitaeigis', 'elektra', 'oblius', 7192, 'labai didelis', 'laisvas', 7331, 94, 4),
(95, '2017-02-03', 'letaeigis', 'dyzelinas', 'kampinis slifuoklis', 9972, 'vidutinis', 'laisvas', 5694, 95, 4),
(96, '2011-04-16', 'greitaeigis', 'elektra', 'spaustuvas', 429, 'didelis', 'isnuomotas', 8511, 96, 2),
(97, '2013-05-31', 'greitaeigis', 'elektra', 'kaltuvas', 2153, 'didelis', 'laisvas', 7230, 97, 3),
(98, '2013-03-14', 'greitaeigis', 'dyzelinas', 'oblius', 918, 'miniatiūrinis', 'remontuojamas', 8336, 98, 1),
(99, '2010-09-08', 'letaeigis', 'benzinas', 'greztuvas', 2185, 'didelis', 'isnuomotas', 8794, 99, 1),
(100, '2015-04-19', 'greitaeigis', 'elektra', 'spaustuvas', 528, 'didelis', 'laisvas', 9921, 100, 4),
(101, '2017-04-01', 'letaeigis', 'benzinas', 'oblius', 2000, 'didelis', 'laisvas', 200, 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `irankiu_grupe`
--

CREATE TABLE `irankiu_grupe` (
  `pavadinimas` varchar(255) COLLATE utf8_lithuanian_ci DEFAULT NULL,
  `id` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci;

--
-- Dumping data for table `irankiu_grupe`
--

INSERT INTO `irankiu_grupe` (`pavadinimas`, `id`) VALUES
('Profesionalus irankiai', 1),
('Megejiski irankiai', 2),
('Vidutines klases irankiai', 3),
('Auksciausios klases irankiai', 4);

-- --------------------------------------------------------

--
-- Table structure for table `klientas`
--

CREATE TABLE `klientas` (
  `asmens_kodas` varchar(255) COLLATE utf8_lithuanian_ci NOT NULL DEFAULT '',
  `vardas` varchar(255) COLLATE utf8_lithuanian_ci DEFAULT NULL,
  `pavarde` varchar(255) COLLATE utf8_lithuanian_ci DEFAULT NULL,
  `gimimo_data` date DEFAULT NULL,
  `telefonas` varchar(255) COLLATE utf8_lithuanian_ci DEFAULT NULL,
  `el_pastas` varchar(255) COLLATE utf8_lithuanian_ci DEFAULT NULL,
  `fk_NAUDOTOJASid` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci;

--
-- Dumping data for table `klientas`
--

INSERT INTO `klientas` (`asmens_kodas`, `vardas`, `pavarde`, `gimimo_data`, `telefonas`, `el_pastas`, `fk_NAUDOTOJASid`) VALUES
('35011229196', 'Adelita ', 'Patrikas', '1966-10-25', '867955741', 'Adelita .Patrikas@gmail.com', 83),
('35111132868', 'Gintvydas', 'Pelagijus', '1955-04-27', '866192786', 'Gintvydas.Pelagijus@gmail.com', 84),
('35311152794', 'Adelė', 'Patijus', '1986-03-15', '857235825', 'Adelė.Patijus@gmail.com', 85),
('35710259343', 'Girkantė', 'Pranciška', '1974-04-10', '868829997', 'Girkantė.Pranciška@gmail.com', 86),
('35711166049', 'Ada', 'Palmira', '1968-04-06', '851758983', 'Ada.Palmira@gmail.com', 87),
('35711215939', 'Girdvilas', 'Polis', '1999-05-11', '859534271', 'Girdvilas.Polis@gmail.com', 88),
('36011116518', 'Girė', 'Poncijus', '1964-04-06', '869796489', 'Girė.Poncijus@gmail.com', 89),
('36110283145', 'Adelaidė', 'Patas', '1968-03-02', '865623782', 'Adelaidė.Patas@gmail.com', 90),
('36210134109', 'Girkantas', 'Pranas', '1977-03-11', '854378396', 'Girkantas.Pranas@gmail.com', 91),
('36211115693', 'Girius', 'Prakseda', '1990-07-04', '863684742', 'Girius.Prakseda@gmail.com', 92),
('36212285748', 'Ginvyda', 'Perlas', '1988-10-19', '863913521', 'Ginvyda.Perlas@gmail.com', 93),
('36410313397', 'Gintautė', 'Pavlas', '1977-12-20', '859849987', 'Gintautė.Pavlas@gmail.com', 94),
('36711276639', 'Gintvilas', 'Perida', '1974-07-17', '863661492', 'Gintvilas.Perida@gmail.com', 95),
('36712148325', 'Adriana', 'Paulius', '1982-08-11', '864389153', 'Adriana.Paulius@gmail.com', 96),
('37010218997', 'Adolfina', 'Paulė', '1973-11-20', '865545465', 'Adolfina.Paulė@gmail.com', 97),
('37210297447', 'Girdvainis', 'Pilypas', '1970-10-13', '855677108', 'Girdvainis.Pilypas@gmail.com', 98),
('37311167712', 'Ginvilas', 'Petrė', '1979-04-27', '865832883', 'Ginvilas.Petrė@gmail.com', 99),
('37411268910', 'Adalberta', 'Panteleonas', '1974-10-29', '858227825', 'Adalberta.Panteleonas@gmail.com', 100),
('37510167595', 'Girdvainė', 'Pilypa', '1991-06-04', '868840215', 'Girdvainė.Pilypa@gmail.com', 101),
('37512179357', 'Girdutė', 'Pijonijus', '1986-06-21', '864977945', 'Girdutė.Pijonijus@gmail.com', 102),
('37810132323', 'Ginvydė', 'Petras', '1978-07-27', '869488643', 'Ginvydė.Petras@gmail.com', 103),
('37811246982', 'Girenė', 'Porta', '1974-04-30', '851426374', 'Girenė.Porta@gmail.com', 104),
('38012114686', 'Girmantė', 'Patrimpas', '1967-04-02', '851033179', 'Girmantė.Patrimpas@gmail.com', 105),
('38111115907', 'Adeodatas', 'Patrimpas', '1985-06-17', '865551269', 'Adeodatas.Patrimpas@gmail.com', 106),
('38112304369', 'Adonis', 'Paulita', '1959-05-24', '857958128', 'Adonis.Paulita@gmail.com', 107),
('38310198416', 'Adelija', 'Patricija', '1962-02-03', '851947468', 'Adelija.Patricija@gmail.com', 108),
('38411246845', 'Giriotas', 'Portas', '1991-12-23', '855776723', 'Giriotas.Portas@gmail.com', 109),
('38412184891', 'Girdenis', 'Pijonija', '1963-06-02', '861956311', 'Girdenis.Pijonija@gmail.com', 110),
('38612123623', 'Adalbertas', 'Paola', '1979-03-05', '868337502', 'Adalbertas.Paola@gmail.com', 111),
('38612159346', 'Gintvyda', 'Pelagija', '1999-02-04', '857347506', 'Gintvyda.Pelagija@gmail.com', 112),
('39212221421', 'Girdvilė', 'Poncijonas', '1959-03-17', '855780913', 'Girdvilė.Poncijonas@gmail.com', 113),
('39312136019', 'Giriotė', 'Povilas', '1981-02-08', '852493198', 'Giriotė.Povilas@gmail.com', 114),
('39410242897', 'Abdonas', 'Pablo', '1998-02-09', '862952191', 'Abdonas.Pablo@gmail.com', 115),
('39411237986', 'Ginvilė', 'Petronėlė', '1991-10-13', '864992524', 'Ginvilė.Petronėlė@gmail.com', 116),
('39411312645', 'Adrianas', 'Pavelas', '1958-07-24', '868482805', 'Adrianas.Pavelas@gmail.com', 117),
('39511136936', 'Adaira', 'Palmyra', '1978-08-02', '857191656', 'Adaira.Palmyra@gmail.com', 118),
('39603020552', 'Raiskelis', 'Raiskas', '2016-01-21', '37065420821', 'dasdf@gmail.com', 119),
('39603020553', 'Marius', 'Raiskas', '2016-01-21', '37065420824', '', 120),
('39603020554', 'Mariusx', 'Marijonaitisx', '2016-01-27', '37065420822', '', 121),
('39603020555', 'Arnas', 'Ambotas', '2016-01-21', '865420822', '', 122),
('39710266249', 'Ginvydas', 'Peteris', '1967-03-23', '861864606', 'Ginvydas.Peteris@gmail.com', 123),
('39812306935', 'Girmantas', 'Pranciškus', '1992-07-29', '856731373', 'Girmantas.Pranciškus@gmail.com', 124),
('45311163838', 'Abigailė', 'Pajauta', '1964-09-26', '854497856', 'Abigailė.Pajauta@gmail.com', 125),
('45411148492', 'Girdenė', 'Petronijus', '1969-06-11', '863120597', 'Girdenė.Petronijus@gmail.com', 126),
('45412185968', 'Gintvilė', 'Perla', '1964-06-11', '863251788', 'Gintvilė.Perla@gmail.com', 127),
('45511288324', 'Adolfa', 'Patris', '1979-07-07', '865176591', 'Adolfa.Patris@gmail.com', 128),
('45911117066', 'Girdvyda', 'Polė', '1985-06-29', '854386772', 'Girdvyda.Polė@gmail.com', 129),
('46211285464', 'Girmintas', 'Patris', '1976-11-12', '858176742', 'Girmintas.Patris@gmail.com', 130),
('46211298826', 'Gintis', 'Pedro', '1979-02-21', '853447527', 'Gintis.Pedro@gmail.com', 131),
('46412151151', 'Adauktas', 'Pastoras', '1960-12-20', '865838265', 'Adauktas.Pastoras@gmail.com', 132),
('46510186409', 'Adolis', 'Paulina', '1990-07-22', '855642243', 'Adolis.Paulina@gmail.com', 133),
('46510267621', 'Girdutis', 'Pijus', '1967-03-20', '866033513', 'Girdutis.Pijus@gmail.com', 134),
('46912317146', 'Abraomas', 'Palemonas', '1961-06-12', '861434752', 'Abraomas.Palemonas@gmail.com', 135),
('47012205024', 'Girdvydė', 'Polina', '1958-03-21', '856696355', 'Girdvydė.Polina@gmail.com', 136),
('47012259078', 'Abrahimas', 'Paladijus', '1995-05-26', '851843975', 'Abrahimas.Paladijus@gmail.com', 137),
('47812132200', 'Adomas', 'Paulinas', '1973-01-26', '865030377', 'Adomas.Paulinas@gmail.com', 138),
('48612234028', 'Girdvydas', 'Polikarpas', '1997-07-10', '865422578', 'Girdvydas.Polikarpas@gmail.com', 139),
('48712248881', 'Gintė', 'Pedras', '1997-07-15', '855149729', 'Gintė.Pedras@gmail.com', 140),
('49010106372', 'Adolfas', 'Paula', '1992-05-11', '854680984', 'Adolfas.Paula@gmail.com', 141),
('49012158209', 'Achilas', 'Palma', '1961-09-04', '852598224', 'Achilas.Palma@gmail.com', 142),
('49311243575', 'Adelina', 'Patricijus', '1959-11-03', '856841505', 'Adelina.Patricijus@gmail.com', 143),
('49510161936', 'Adas', 'Paskalis', '1986-12-20', '853435395', 'Adas.Paskalis@gmail.com', 144),
('49710272154', 'Girėnas', 'Pontius', '1974-04-11', '866367524', 'Girėnas.Pontius@gmail.com', 145),
('49712165301', 'Gintvydė', 'Penelopė', '1999-08-04', '862026363', 'Gintvydė.Penelopė@gmail.com', 146);

-- --------------------------------------------------------

--
-- Table structure for table `miestas`
--

CREATE TABLE `miestas` (
  `pavadinimas` varchar(255) COLLATE utf8_lithuanian_ci DEFAULT NULL,
  `id` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci;

--
-- Dumping data for table `miestas`
--

INSERT INTO `miestas` (`pavadinimas`, `id`) VALUES
('Kaunas', 1),
('Vilnius', 2),
('Kedainiai', 3),
('Palanga', 4),
('Taurage', 5);

-- --------------------------------------------------------

--
-- Table structure for table `modelis`
--

CREATE TABLE `modelis` (
  `pavadinimas` varchar(255) COLLATE utf8_lithuanian_ci DEFAULT NULL,
  `id` int(11) NOT NULL DEFAULT '0',
  `fk_FIRMAid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci;

--
-- Dumping data for table `modelis`
--

INSERT INTO `modelis` (`pavadinimas`, `id`, `fk_FIRMAid`) VALUES
('PQK6297', 1, 5),
('QTY3721', 2, 1),
('AEZ8774', 3, 11),
('YOT3596', 4, 1),
('XLZ3560', 5, 1),
('OSQ7713', 6, 12),
('NZX7099', 7, 13),
('OGD8073', 8, 1),
('DJU8297', 9, 1),
('KTS1646', 10, 1),
('TCA7476', 11, 2),
('XJU5565', 12, 2),
('XTY6672', 13, 12),
('RFM3198', 14, 2),
('IOU532', 15, 2),
('MYH4401', 16, 2),
('HFV118', 17, 2),
('XWJ7267', 18, 3),
('YJI5511', 19, 3),
('FZR2144', 20, 3),
('VAB2146', 21, 3),
('BVW5129', 22, 3),
('KKY7797', 23, 3),
('MVI5341', 24, 13),
('HJI6715', 25, 3),
('BDO6813', 26, 4),
('NKB6586', 27, 14),
('AUZ6567', 28, 4),
('PLG5704', 29, 4),
('RNU7521', 30, 4),
('FPO485', 31, 4),
('ZLT5662', 32, 4),
('EHT3178', 33, 5),
('CSR5982', 34, 15),
('RIN1642', 35, 5),
('DLK6866', 36, 5),
('TXB8976', 37, 5),
('DYM1368', 38, 5),
('VGB2251', 39, 5),
('YYF4600', 40, 16),
('RTO327', 41, 6),
('TYM3214', 42, 6),
('ZGE8402', 43, 6),
('ARR8374', 44, 6),
('FKP4778', 45, 6),
('MEB1566', 46, 6),
('SEE3822', 47, 6),
('NFS3046', 48, 7),
('JJM4757', 49, 7),
('RBA976', 50, 7),
('UEM3937', 51, 7),
('SEE8049', 52, 7),
('FUV922', 53, 7),
('GCB4600', 54, 7),
('GHB7423', 55, 7),
('PRA6667', 56, 8),
('MSM1116', 57, 8),
('HMZ653', 58, 8),
('OLW5576', 59, 8),
('WJI5017', 60, 8),
('OCD261', 61, 8),
('YUN4623', 62, 8),
('OLQ5579', 63, 8),
('ZXF9922', 64, 9),
('UJD2207', 65, 9),
('NLA4300', 66, 9),
('VHY7743', 67, 9),
('LWP3301', 68, 9),
('UEG8466', 69, 9),
('UDC710', 70, 9),
('DQO536', 71, 9),
('TYQ3025', 72, 9),
('YRV110', 73, 10),
('SBY7349', 74, 11),
('AAZ2280', 75, 11),
('DAY4430', 76, 12),
('OEQ9662', 77, 12),
('EYS3987', 78, 13),
('JZO1239', 79, 13),
('ULJ1872', 80, 14),
('RTO3261', 81, 14),
('NZP2144', 82, 14),
('YYW503', 83, 15),
('YBP673', 84, 15),
('BGZ3219', 85, 16),
('RGW5827', 86, 16),
('PAJ3036', 87, 16),
('NDL9850', 88, 16),
('VQJ2336', 89, 17),
('JWJ8075', 90, 17),
('JBT7408', 91, 17),
('FYG5818', 92, 18),
('CKU1583', 93, 18),
('USO6201', 94, 18),
('AQI869', 95, 18),
('KXQ2902', 96, 19),
('NFM7821', 97, 19),
('HMG8958', 98, 19),
('HDA2193', 99, 20),
('URZ7187', 100, 20),
('MX324234', 101, 3),
('SKX30234234', 102, 6);

-- --------------------------------------------------------

--
-- Table structure for table `mokejimas`
--

CREATE TABLE `mokejimas` (
  `data` date DEFAULT NULL,
  `suma` float DEFAULT NULL,
  `id` int(11) NOT NULL DEFAULT '0',
  `fk_KLIENTASasmens_kodas` varchar(255) COLLATE utf8_lithuanian_ci NOT NULL,
  `fk_SASKAITAnr` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci;

--
-- Dumping data for table `mokejimas`
--

INSERT INTO `mokejimas` (`data`, `suma`, `id`, `fk_KLIENTASasmens_kodas`, `fk_SASKAITAnr`) VALUES
('2016-08-26', 1696, 1, '39410242897', 1),
('2017-02-14', 730, 2, '45311163838', 2),
('2016-12-09', 731, 3, '47012259078', 3),
('2017-04-08', 1555, 4, '46912317146', 4),
('2017-01-15', 1676, 5, '49012158209', 5),
('2017-02-04', 677, 6, '35711166049', 6),
('2016-09-08', 2368, 7, '39511136936', 7),
('2017-02-20', 1932, 8, '37411268910', 8),
('2016-12-20', 394, 9, '38612123623', 9),
('2017-03-06', 1597, 10, '49510161936', 10),
('2016-12-03', 2102, 11, '46412151151', 11),
('2017-05-01', 260, 12, '36110283145', 12),
('2017-02-25', 1369, 13, '35311152794', 13),
('2016-11-22', 1997, 14, '38310198416', 14),
('2016-09-18', 1996, 15, '49311243575', 15),
('2016-11-16', 1810, 16, '35011229196', 16),
('2016-09-09', 2034, 17, '38111115907', 17),
('2016-09-21', 696, 18, '45511288324', 18),
('2017-01-26', 922, 19, '49010106372', 19),
('2017-02-18', 2151, 20, '37010218997', 20),
('2016-10-31', 516, 21, '46510186409', 21),
('2016-12-06', 336, 22, '47812132200', 22),
('2017-04-15', 2284, 23, '38112304369', 23),
('2017-04-14', 2167, 24, '36712148325', 24),
('2016-11-15', 1531, 25, '39411312645', 25),
('2016-10-02', 1350, 26, '36410313397', 26),
('2016-11-04', 2062, 27, '48712248881', 27),
('2017-04-16', 1687, 28, '46211298826', 28),
('2016-08-23', 708, 29, '38612159346', 29),
('2017-04-28', 344, 30, '35111132868', 30);

-- --------------------------------------------------------

--
-- Table structure for table `mokestis`
--

CREATE TABLE `mokestis` (
  `pavadinimas` varchar(255) COLLATE utf8_lithuanian_ci DEFAULT NULL,
  `aprasymas` varchar(255) COLLATE utf8_lithuanian_ci DEFAULT NULL,
  `id` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci;

--
-- Dumping data for table `mokestis`
--

INSERT INTO `mokestis` (`pavadinimas`, `aprasymas`, `id`) VALUES
('Valymas', 'Irankio nuvalymas, prekines isvaizdos atstatymas', 1),
('Nusidevejimas', 'Irankio prieziura, remonto kastu kaupimas', 2),
('Apziura', 'Irankio apziura po naudojimo, nusidevejimo Ivertinimas', 3),
('Pristatymo islaidos', 'Papildomos islaidos pristatant iranki', 4);

-- --------------------------------------------------------

--
-- Table structure for table `naudotojas`
--

CREATE TABLE `naudotojas` (
  `id` int(11) NOT NULL,
  `el_pastas` varchar(255) COLLATE utf8_lithuanian_ci NOT NULL,
  `slaptazodis` varchar(255) COLLATE utf8_lithuanian_ci NOT NULL,
  `fk_NAUDOTOJO_BUSENAid` int(11) NOT NULL,
  `fk_NAUDOTOJO_ROLEid` int(11) NOT NULL,
  `sukurimo_data` datetime NOT NULL,
  `redagavimo_data` datetime DEFAULT NULL,
  `prisijungimo_data` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci;

--
-- Dumping data for table `naudotojas`
--

INSERT INTO `naudotojas` (`id`, `el_pastas`, `slaptazodis`, `fk_NAUDOTOJO_BUSENAid`, `fk_NAUDOTOJO_ROLEid`, `sukurimo_data`, `redagavimo_data`, `prisijungimo_data`) VALUES
(1, 'jonax.jonaitis@mydomain.com', 'a7e9312a47b29ab95f7264dd058617b4', 1, 2, '2017-09-30 12:26:00', NULL, NULL),
(2, 'mariusx.marijonaitisx@mydomain.com', 'a11b66b6426d8c849ac593a546562869', 1, 2, '2017-09-30 12:26:00', NULL, NULL),
(3, 'paulius.pauliukevicius@mydomain.com', '582885eee5921cbcb2bf059781b81314', 1, 2, '2017-09-30 12:26:00', NULL, NULL),
(4, 'lukas.lukauskis@mydomain.com', 'e116a5817bf6e17dd85f6ccbc8b301a8', 1, 2, '2017-09-30 12:26:00', NULL, NULL),
(5, 'rokas.rokauskis@mydomain.com', 'e116a5817bf6e17dd85f6ccbc8b301a8', 1, 2, '2017-09-30 12:26:00', NULL, NULL),
(6, 'saulius.sauliuskevicius@mydomain.com', '3119b6d2f8037b5ae0db1c03b324ac47', 1, 2, '2017-09-30 12:26:00', NULL, NULL),
(7, 'martynas.martinkonas@mydomain.com', '67b710367782dbaaac248dd85280b2df', 1, 2, '2017-09-30 12:26:00', NULL, NULL),
(8, 'martis.butautas@mydomain.com', '4863bc8bbfae1426ba0fc526c327f2e6', 1, 2, '2017-09-30 12:26:00', NULL, NULL),
(9, 'marcius.radzevicius@mydomain.com', 'a347a809ded41e14d400351fe7dd259e', 1, 2, '2017-09-30 12:26:00', NULL, NULL),
(10, 'juozapas.dainiupras@mydomain.com', '9a10513e854f75485ab05537e1dfc63f', 1, 2, '2017-09-30 12:26:00', NULL, NULL),
(11, 'juozas.dainiuskevicius@mydomain.com', 'e75dfaa4546184a8efec2ee0bd556300', 1, 2, '2017-09-30 12:26:00', NULL, NULL),
(12, 'arnas.milaknis@mydomain.com', '7e2a432ac85d34600b0393438c065806', 1, 2, '2017-09-30 12:26:00', NULL, NULL),
(13, 'arnoldas.jasikevicius@mydomain.com', 'b4c6e38a26549cff54713d109420e534', 1, 2, '2017-09-30 12:26:00', NULL, NULL),
(14, 'steponas.marciulionis@mydomain.com', '3d573a817a4d97c38faf066e50ffb25b', 1, 2, '2017-09-30 12:26:00', NULL, NULL),
(15, 'stepas.garastas@mydomain.com', 'a3077c4f6041de69c97c201fd2a590f6', 1, 2, '2017-09-30 12:26:00', NULL, NULL),
(16, 'povilas.bakchis@mydomain.com', '9b5d46158b114bb3aaaa55f863095a34', 1, 2, '2017-09-30 12:26:00', NULL, NULL),
(17, 'ilona.valaityte@mydomain.com', 'e5d83e13fbbfa0481db9757a13f4287c', 1, 2, '2017-09-30 12:26:00', NULL, NULL),
(18, 'ruta.kutka@mydomain.com', '358351a657dada8f0fd16c1cf7eee9a8', 1, 2, '2017-09-30 12:26:00', NULL, NULL),
(19, 'rasa.draugelaite@mydomain.com', '730b5a0485606674720847c323b330db', 1, 2, '2017-09-30 12:26:00', NULL, NULL),
(20, 'giedrius.brilius@mydomain.com', '00268dd8ff44c26870be201ca455e3dd', 1, 2, '2017-09-30 12:26:00', NULL, NULL),
(21, 'ernestas.anuprevicius@mydomain.com', 'e9a8f67c3a4368697d63652f634442cd', 1, 2, '2017-09-30 12:26:00', NULL, NULL),
(22, 'ernvaldas.dainiauskas@mydomain.com', '51f4d5cb1c59de9921e14da96d7c2947', 1, 2, '2017-09-30 12:26:00', NULL, NULL),
(23, 'dainius.virsiliskis@mydomain.com', '8612bff8b73d002db65b19263c000598', 1, 2, '2017-09-30 12:26:00', NULL, NULL),
(24, 'dainora.dainoriene@mydomain.com', '115cc0d612f6fd57eab281d8a5f5b860', 1, 2, '2017-09-30 12:26:00', NULL, NULL),
(25, 'jonanas.valaitis@mydomain.com', '91784caf1d19858628222c025068a4ed', 1, 2, '2017-09-30 12:26:00', NULL, NULL),
(26, 'dijora.joskiene@mydomain.com', 'c548e9c1376d4f0d7b58172555a38d44', 1, 2, '2017-09-30 12:26:00', NULL, NULL),
(27, 'darijonas.juskevicius@mydomain.com', '9bdb9239815f27d6da01a2fcddf54b6c', 1, 2, '2017-09-30 12:26:00', NULL, NULL),
(28, 'faustas.janavicius@mydomain.com', 'fbfe23c220d632e7ad55321e335a0eb1', 1, 2, '2017-09-30 12:26:00', NULL, NULL),
(29, 'faustina.kutkaite@mydomain.com', '8d61422146e8a4fb2ab00a5146b3c41f', 1, 2, '2017-09-30 12:26:00', NULL, NULL),
(30, 'giedre.jurgeleviciute@mydomain.com', '989fe9c0e43547df9795f7b78c054179', 1, 2, '2017-09-30 12:26:00', NULL, NULL),
(31, 'zadvyde.talius@mydomain.com', '436f07be37f7d70de7a38822ddd26be9', 1, 2, '2017-09-30 12:26:00', NULL, NULL),
(32, 'zakas.tamara@mydomain.com', '216a00bd9f66b72e0a81dfbead7ad4d9', 1, 2, '2017-09-30 12:26:00', NULL, NULL),
(33, 'zaklina.tania@mydomain.com', '577e2a1db3c4cac0fd1f7bfeb20f3b81', 1, 2, '2017-09-30 12:26:00', NULL, NULL),
(34, 'zana.tanvilas@mydomain.com', 'eafd82719b186461fe934e951f551486', 1, 2, '2017-09-30 12:26:00', NULL, NULL),
(35, 'zanas.tanvile@mydomain.com', '04c845002f044de5703e46afa2b9a4fb', 1, 2, '2017-09-30 12:26:00', NULL, NULL),
(36, 'zaneta.tarena@mydomain.com', '844a47a42108610754b59572d8cff8c7', 1, 2, '2017-09-30 12:26:00', NULL, NULL),
(37, 'zavinta.tarenas@mydomain.com', '04e966bc86ef51d91548451053bf83d0', 1, 2, '2017-09-30 12:26:00', NULL, NULL),
(38, 'zeimantas.tarmantas@mydomain.com', '8fb296c8d2d5b8c587f0b02d985156c3', 1, 2, '2017-09-30 12:26:00', NULL, NULL),
(39, 'zeimante.tarmante@mydomain.com', '83d668a41d548ba05e12399ff81a675e', 1, 2, '2017-09-30 12:26:00', NULL, NULL),
(40, 'zemyna.tarvainas@mydomain.com', 'cd11b849705e2e7c4691d485455fa1d1', 1, 2, '2017-09-30 12:26:00', NULL, NULL),
(41, 'zenevjeva.tarvaine@mydomain.com', '1342e28c248aa04bb1db4dfddd80064f', 1, 2, '2017-09-30 12:26:00', NULL, NULL),
(42, 'zeraldas.tarvilas@mydomain.com', 'b09a76f6fc14716f6b517e6808d0de5a', 1, 2, '2017-09-30 12:26:00', NULL, NULL),
(43, 'zermena.tarvile@mydomain.com', '801f3c992adb75370b085ae2ffb325c0', 1, 2, '2017-09-30 12:26:00', NULL, NULL),
(44, 'zerute.tarvina@mydomain.com', 'c90bd1fe92a4fbea65a51dae9f0fd1d5', 1, 2, '2017-09-30 12:26:00', NULL, NULL),
(45, 'zerutis.tarvinas@mydomain.com', '5d69b88b0af216bed90c4bc2c9eba002', 1, 2, '2017-09-30 12:26:00', NULL, NULL),
(46, 'zybartas.tatjana@mydomain.com', '53979a7c6faaa59ea9189ebbea796978', 1, 2, '2017-09-30 12:26:00', NULL, NULL),
(47, 'zybarte.tauras@mydomain.com', '3f388e2c718669c99ec6973448be1eba', 1, 2, '2017-09-30 12:26:00', NULL, NULL),
(48, 'zibuokle.taure@mydomain.com', '8f91e990d19dc388333eb6ba70d0e2ed', 1, 2, '2017-09-30 12:26:00', NULL, NULL),
(49, 'zibute.taurija@mydomain.com', '81d1e2fa8bcc8fe4c3b89e7af2fd5f53', 1, 2, '2017-09-30 12:26:00', NULL, NULL),
(50, 'zydre.taurijus@mydomain.com', '5adc5768eb908b23a3009fedf415d32a', 1, 2, '2017-09-30 12:26:00', NULL, NULL),
(51, 'zydrius.taurys@mydomain.com', '49890f90fa5a8f34000b71a0dc2803d6', 1, 2, '2017-09-30 12:26:00', NULL, NULL),
(52, 'zydrunas.taurius@mydomain.com', '84805ce9c22e303ed60e072b78e0a283', 1, 2, '2017-09-30 12:26:00', NULL, NULL),
(53, 'zydrune.tautas@mydomain.com', 'e6c43f27c5b21075dfa15d610a3f2d14', 1, 2, '2017-09-30 12:26:00', NULL, NULL),
(54, 'zydrute.taute@mydomain.com', '61e2b0125a309504179fcb6a32781d78', 1, 2, '2017-09-30 12:26:00', NULL, NULL),
(55, 'zydrutis.tautene@mydomain.com', 'fc0fa7947a45d9e89c5324baef17a83b', 1, 2, '2017-09-30 12:26:00', NULL, NULL),
(56, 'ziede.tautenis@mydomain.com', 'dd1ddc5f0a306af9e197979555f2af19', 1, 2, '2017-09-30 12:26:00', NULL, NULL),
(57, 'zygaudas.tautgailas@mydomain.com', 'ddf11104a08ce2f631fd04a7a8f8c882', 1, 2, '2017-09-30 12:26:00', NULL, NULL),
(58, 'zygaude.tautgaile@mydomain.com', '414aa594cfc9557ae0a2b69c50508867', 1, 2, '2017-09-30 12:26:00', NULL, NULL),
(59, 'zygimantas.tautgina@mydomain.com', '41e862f2d5d34429bec24accef0c1eee', 1, 2, '2017-09-30 12:26:00', NULL, NULL),
(60, 'zygimante.tautginas@mydomain.com', 'f79e8f2fc8dfe2e0e79ff6090ac43378', 1, 2, '2017-09-30 12:26:00', NULL, NULL),
(61, 'zyginta.tautgine@mydomain.com', 'a4a6fd445c750017cd46ca113d7c4d01', 1, 2, '2017-09-30 12:26:00', NULL, NULL),
(62, 'zygintas.tautginta@mydomain.com', '68c2c19efe836d78b63b348cbcb61ae6', 1, 2, '2017-09-30 12:26:00', NULL, NULL),
(63, 'zyginte.tautgintas@mydomain.com', '107838f91d107319b1064f355ddf6a8d', 1, 2, '2017-09-30 12:26:00', NULL, NULL),
(64, 'zygis.tautginte@mydomain.com', '0b1df312678e982fc391d38e599f5246', 1, 2, '2017-09-30 12:26:00', NULL, NULL),
(65, 'zygmantas.tautgirdas@mydomain.com', '269f977590227c6fdbed7d5b9d46fb71', 1, 2, '2017-09-30 12:26:00', NULL, NULL),
(66, 'zygmante.tautgirde@mydomain.com', 'e67f362146c640eed4236280006e337b', 1, 2, '2017-09-30 12:26:00', NULL, NULL),
(67, 'zygunas.tautkanta@mydomain.com', '4156e30ba2d04d84f263d6f10ccb906a', 1, 2, '2017-09-30 12:26:00', NULL, NULL),
(68, 'zilvija.tautkantas@mydomain.com', 'ca028444004fbbc6575560df6d8be1a6', 1, 2, '2017-09-30 12:26:00', NULL, NULL),
(69, 'zilvijus.tautmilas@mydomain.com', '780046bd1046fb6a47fbcf30d7c99d35', 1, 2, '2017-09-30 12:26:00', NULL, NULL),
(70, 'zilvinas.tautmile@mydomain.com', '1c1f6acc2eec4f84fec412f429b54112', 1, 2, '2017-09-30 12:26:00', NULL, NULL),
(71, 'zymantas.raiskas@mydomain.com', '21d58306bee260900c1c0e12b7ace284', 1, 2, '2017-09-30 12:26:00', NULL, NULL),
(72, 'zymante.tautrime@mydomain.com', '8e3a15857e3a5987a34849ee70c693e8', 1, 2, '2017-09-30 12:26:00', NULL, NULL),
(73, 'zintautas.tautvaldas@mydomain.com', 'ade58df40c8f75813a7d7573af1c1f0c', 1, 2, '2017-09-30 12:26:00', NULL, NULL),
(74, 'zintaute.tautvalde@mydomain.com', '84b83911bb9f45cc6641b380424c67fe', 1, 2, '2017-09-30 12:26:00', NULL, NULL),
(75, 'zyvilas.tautve@mydomain.com', '1fd03630b7a2af8427fe2c54d663ecd2', 1, 2, '2017-09-30 12:26:00', NULL, NULL),
(76, 'zivile.tautvyda@mydomain.com', 'fe298808517866e88bb3950b87b00b03', 1, 2, '2017-09-30 12:26:00', NULL, NULL),
(77, 'zyvile.tautvydas@mydomain.com', '88147f3c289d828d1502f60c7605ad4e', 1, 2, '2017-09-30 12:26:00', NULL, NULL),
(78, 'zivilis.tautvyde@mydomain.com', '7b5f088c237939667c413e9dc55f0106', 1, 2, '2017-09-30 12:26:00', NULL, NULL),
(79, 'zvainibudas.tautvilas@mydomain.com', 'a2847d79c60e1e26ce82c5afb76f54ae', 1, 2, '2017-09-30 12:26:00', NULL, NULL),
(80, 'aurelijus.veryga@mydomain.com', 'a8b9f9fbd26b5e3e403b501aaab54ecf', 1, 2, '2017-09-30 12:26:00', NULL, NULL),
(81, 'rokas.raiskas@mydomain.com', 'bda92d141ac328ffb78ec2310dae9feb', 1, 2, '2017-09-30 12:26:00', NULL, NULL),
(82, 'arnas.ambotas@mydomain.com', '24b1447bc07a4f5d050a4cd79f097508', 1, 2, '2017-09-30 12:26:00', NULL, NULL),
(83, 'adelita .patrikas@mydomain.com', 'b2a96b587675d532783f628c6b9f2290', 1, 1, '2017-09-30 12:46:00', NULL, NULL),
(84, 'gintvydas.pelagijus@mydomain.com', '7a5d630b463862d5b64e7464f99000a9', 2, 1, '2017-09-30 12:46:00', NULL, NULL),
(85, 'adele.patijus@mydomain.com', 'be07b301b984812c14c08b8402220f1e', 1, 1, '2017-09-30 12:46:00', NULL, NULL),
(86, 'girkante.pranciska@mydomain.com', '0e5f1f0db86ec7edda5f61913f678af7', 1, 1, '2017-09-30 12:46:00', NULL, NULL),
(87, 'ada.palmira@mydomain.com', '83469ed2521f07cb27804061cf244132', 1, 1, '2017-09-30 12:46:00', NULL, NULL),
(88, 'girdvilas.polis@mydomain.com', 'd991b0418cf25b029e1571c7b7aae4c8', 1, 1, '2017-09-30 12:46:00', NULL, NULL),
(89, 'gire.poncijus@mydomain.com', '3176a1dbc8a2366308e630ef2baa8ea9', 1, 1, '2017-09-30 12:46:00', NULL, NULL),
(90, 'adelaide.patas@mydomain.com', 'b538ef3793114c8eaea86c934cf6573f', 1, 1, '2017-09-30 12:46:00', NULL, NULL),
(91, 'girkantas.pranas@mydomain.com', '0030abf837102cc24c16530eb996adee', 1, 1, '2017-09-30 12:46:00', NULL, NULL),
(92, 'girius.prakseda@mydomain.com', '9ff185587ea8882e2a5f033f556b34cc', 1, 1, '2017-09-30 12:46:00', NULL, NULL),
(93, 'ginvyda.perlas@mydomain.com', 'bbbcaaa5e2af692a5f89d9d595004e5e', 1, 1, '2017-09-30 12:46:00', NULL, NULL),
(94, 'gintaute.pavlas@mydomain.com', 'd4e7edeafdf4b2da207de415785c3f82', 1, 1, '2017-09-30 12:46:00', NULL, NULL),
(95, 'gintvilas.perida@mydomain.com', '465ac109f407db7b23812ab58eac532a', 1, 1, '2017-09-30 12:46:00', NULL, NULL),
(96, 'adriana.paulius@mydomain.com', '4177cc4f52b85469c6757a414c493309', 1, 1, '2017-09-30 12:46:00', NULL, NULL),
(97, 'adolfina.paule@mydomain.com', '36887edd80a96b99f787956a578c71e5', 1, 1, '2017-09-30 12:46:00', NULL, NULL),
(98, 'girdvainis.pilypas@mydomain.com', 'cd6db3eccfd3b87ad7c391374ee87f3a', 1, 1, '2017-09-30 12:46:00', NULL, NULL),
(99, 'ginvilas.petre@mydomain.com', '1a1945ff75c621424ec5ba26813e4b4c', 1, 1, '2017-09-30 12:46:00', NULL, NULL),
(100, 'adalberta.panteleonas@mydomain.com', '56cf342a27ba44e0b281e1a87e2b8b70', 1, 1, '2017-09-30 12:46:00', NULL, NULL),
(101, 'girdvaine.pilypa@mydomain.com', '7e6b4d31450b66c22cf40ea06f487e77', 1, 1, '2017-09-30 12:46:00', NULL, NULL),
(102, 'girdute.pijonijus@mydomain.com', '836473f3f3fa9611c91e71100c7a987e', 1, 1, '2017-09-30 12:46:00', NULL, NULL),
(103, 'ginvyde.petras@mydomain.com', 'a65bb871480249ede2fe9d74be42bfe3', 1, 1, '2017-09-30 12:46:00', NULL, NULL),
(104, 'girene.porta@mydomain.com', 'fddc93a8c435c26c2618de1be800cd76', 1, 1, '2017-09-30 12:46:00', NULL, NULL),
(105, 'girmante.patrimpas@mydomain.com', 'f9b46c903c610ea4854b7f538de313c9', 1, 1, '2017-09-30 12:46:00', NULL, NULL),
(106, 'adeodatas.patrimpas@mydomain.com', '4ec503bbd83deb097d5b3c8d9bfb1ca9', 1, 1, '2017-09-30 12:46:01', NULL, NULL),
(107, 'adonis.paulita@mydomain.com', '51cbdeb068136bf84cb9f7c33b7939ae', 1, 1, '2017-09-30 12:46:01', NULL, NULL),
(108, 'adelija.patricija@mydomain.com', '87db223524ff9b59c92e06b6c745dfc9', 1, 1, '2017-09-30 12:46:01', NULL, NULL),
(109, 'giriotas.portas@mydomain.com', '25c375238e86b72a074db28377328215', 1, 1, '2017-09-30 12:46:01', NULL, NULL),
(110, 'girdenis.pijonija@mydomain.com', '9603f1c936bafcf126febd81fd99c3db', 1, 1, '2017-09-30 12:46:01', NULL, NULL),
(111, 'adalbertas.paola@mydomain.com', '197f4ebdf16621388bb39417d1c0c8f7', 1, 1, '2017-09-30 12:46:01', NULL, NULL),
(112, 'gintvyda.pelagija@mydomain.com', 'c5d730d27e3b0c51d96b355c1f314ede', 1, 1, '2017-09-30 12:46:01', NULL, NULL),
(113, 'girdvile.poncijonas@mydomain.com', '094eeca13fe5dacce754e7d372e5bade', 1, 1, '2017-09-30 12:46:01', NULL, NULL),
(114, 'giriote.povilas@mydomain.com', 'e1c1aa4ced3dd6e48223765ef42d75d4', 1, 1, '2017-09-30 12:46:01', NULL, NULL),
(115, 'abdonas.pablo@mydomain.com', '98801581601e2a094670db95c6f2a21e', 1, 1, '2017-09-30 12:46:01', NULL, NULL),
(116, 'ginvile.petronele@mydomain.com', 'd01b2ba412c57c6b1099f099088e7661', 1, 1, '2017-09-30 12:46:01', NULL, NULL),
(117, 'adrianas.pavelas@mydomain.com', '32a800aeca8eb8f547fb42c497ee19aa', 1, 1, '2017-09-30 12:46:01', NULL, NULL),
(118, 'adaira.palmyra@mydomain.com', 'f5bb7633bb4d387d8157d997b25abfdf', 1, 1, '2017-09-30 12:46:01', NULL, NULL),
(119, 'raiskelis.raiskas@mydomain.com', '92ec151019024ae60f3c0195d9c7d325', 1, 1, '2017-09-30 12:46:01', NULL, NULL),
(120, 'marius.raiskas@mydomain.com', '4676cf918fc8f0f8a0a7770d9f31db0e', 1, 1, '2017-09-30 12:46:01', NULL, NULL),
(121, 'mariusx.marijonaitisx@mydomain.com', 'a11b66b6426d8c849ac593a546562869', 1, 1, '2017-09-30 12:46:01', NULL, NULL),
(122, 'arnas.ambotas@mydomain.com', '24b1447bc07a4f5d050a4cd79f097508', 1, 1, '2017-09-30 12:46:01', NULL, NULL),
(123, 'ginvydas.peteris@mydomain.com', '614415ba3ec067a4563a1fceeef87a55', 1, 1, '2017-09-30 12:46:01', NULL, NULL),
(124, 'girmantas.pranciskus@mydomain.com', 'eda01e0559aa75ca53e44bfa3b30f670', 1, 1, '2017-09-30 12:46:01', NULL, NULL),
(125, 'abigaile.pajauta@mydomain.com', 'e19f6c005e023c7cf61e81ec3700c0a1', 1, 1, '2017-09-30 12:46:01', NULL, NULL),
(126, 'girdene.petronijus@mydomain.com', 'ba1378c3dab16369dbb17d20931de090', 1, 1, '2017-09-30 12:46:01', NULL, NULL),
(127, 'gintvile.perla@mydomain.com', '9f926960824147808907b151928b0ec4', 1, 1, '2017-09-30 12:46:01', NULL, NULL),
(128, 'adolfa.patris@mydomain.com', '350d38cc6d9903c27501f6b35e2a8a02', 1, 1, '2017-09-30 12:46:01', NULL, NULL),
(129, 'girdvyda.pole@mydomain.com', '7c8dd4aaf9844c45940db39f4b73f8c5', 1, 1, '2017-09-30 12:46:01', NULL, NULL),
(130, 'girmintas.patris@mydomain.com', '852c7a2419d49eab2c2fd7ab0d072731', 1, 1, '2017-09-30 12:46:01', NULL, NULL),
(131, 'gintis.pedro@mydomain.com', '5d4d40c291de467285e601c9235bdde1', 1, 1, '2017-09-30 12:46:01', NULL, NULL),
(132, 'adauktas.pastoras@mydomain.com', 'a143d1704428c53041a838973e7d2d61', 1, 1, '2017-09-30 12:46:01', NULL, NULL),
(133, 'adolis.paulina@mydomain.com', '1f547df556706dcaf598df91aea7aef6', 1, 1, '2017-09-30 12:46:01', NULL, NULL),
(134, 'girdutis.pijus@mydomain.com', '1fc96c49bff35f473160b9e74919ea59', 1, 1, '2017-09-30 12:46:01', NULL, NULL),
(135, 'abraomas.palemonas@mydomain.com', '93aed8349aa544f418de9aa99d8d73a7', 1, 1, '2017-09-30 12:46:01', NULL, NULL),
(136, 'girdvyde.polina@mydomain.com', 'd4eb0049d3f4c8481165ff5228a26295', 1, 1, '2017-09-30 12:46:01', NULL, NULL),
(137, 'abrahimas.paladijus@mydomain.com', 'f908ccee0638c064998c45e84a802086', 1, 1, '2017-09-30 12:46:01', NULL, NULL),
(138, 'adomas.paulinas@mydomain.com', 'e0b2cd4bc3f8ae145f084d7f5bd48c83', 1, 1, '2017-09-30 12:46:01', NULL, NULL),
(139, 'girdvydas.polikarpas@mydomain.com', '9bd08b540f971a95677fafaeb3b5a02d', 1, 1, '2017-09-30 12:46:01', NULL, NULL),
(140, 'ginte.pedras@mydomain.com', '393186cd5dc4ea640c607d3723004a72', 1, 1, '2017-09-30 12:46:01', NULL, NULL),
(141, 'adolfas.paula@mydomain.com', 'c2ac2fa45adf3eecc890c0aea9191583', 1, 1, '2017-09-30 12:46:01', NULL, NULL),
(142, 'achilas.palma@mydomain.com', '140c24927e91f4ced0aba2f4c88c56ed', 1, 1, '2017-09-30 12:46:01', NULL, NULL),
(143, 'adelina.patricijus@mydomain.com', '28a555655170dc50ddc758681c78342c', 1, 1, '2017-09-30 12:46:01', NULL, NULL),
(144, 'adas.paskalis@mydomain.com', '870da36fc40bc5689f5423da492f4826', 1, 1, '2017-09-30 12:46:01', NULL, NULL),
(145, 'girenas.pontius@mydomain.com', '32f338f0710e0d3ce2691c2591552cd8', 1, 1, '2017-09-30 12:46:01', NULL, NULL),
(146, 'gintvyde.penelope@mydomain.com', 'dfeed7e98f256b209536f813f42892c6', 1, 1, '2017-09-30 12:46:01', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `naudotojo_busena`
--

CREATE TABLE `naudotojo_busena` (
  `id` int(11) NOT NULL,
  `pavadinimas` varchar(255) COLLATE utf8_lithuanian_ci NOT NULL,
  `komentaras` varchar(500) COLLATE utf8_lithuanian_ci DEFAULT NULL,
  `galima_prisijungti` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci;

--
-- Dumping data for table `naudotojo_busena`
--

INSERT INTO `naudotojo_busena` (`id`, `pavadinimas`, `komentaras`, `galima_prisijungti`) VALUES
(1, 'Aktyvus', 'Turi teise prisijungti', 1),
(2, 'Blokuotas', 'Neturi teises prisijungti', 0),
(3, 'Ištrintas', 'Neturi teisės prisijungti, nerodomas sąršuose', 0);

-- --------------------------------------------------------

--
-- Table structure for table `naudotojo_role`
--

CREATE TABLE `naudotojo_role` (
  `id` int(11) NOT NULL,
  `pavadinimas` varchar(255) COLLATE utf8_lithuanian_ci NOT NULL,
  `komentaras` varchar(500) COLLATE utf8_lithuanian_ci DEFAULT NULL,
  `darbuotojas` tinyint(1) NOT NULL,
  `administratorius` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci;

--
-- Dumping data for table `naudotojo_role`
--

INSERT INTO `naudotojo_role` (`id`, `pavadinimas`, `komentaras`, `darbuotojas`, `administratorius`) VALUES
(1, 'Klientas', 'Turi teisę peržiūrėti savo informaciją', 0, 0),
(2, 'Darbuotojas', 'Turi teisę įvesti klientą, suformuoti sutartį', 1, 0),
(3, 'Vadovas', 'Darbuotojas turintis visas teises', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `nuomos_biuras`
--

CREATE TABLE `nuomos_biuras` (
  `pavadinimas` varchar(255) COLLATE utf8_lithuanian_ci DEFAULT NULL,
  `adresas` varchar(255) COLLATE utf8_lithuanian_ci DEFAULT NULL,
  `telefonas` varchar(255) COLLATE utf8_lithuanian_ci DEFAULT NULL,
  `el_pastas` varchar(255) COLLATE utf8_lithuanian_ci DEFAULT NULL,
  `id` int(11) NOT NULL DEFAULT '0',
  `fk_MIESTASid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci;

--
-- Dumping data for table `nuomos_biuras`
--

INSERT INTO `nuomos_biuras` (`pavadinimas`, `adresas`, `telefonas`, `el_pastas`, `id`, `fk_MIESTASid`) VALUES
('Neris', 'Kreves 5', '865420225', 'neris@irankiunuoma.lt', 1, 1),
('Nemunas', 'Partizanu 6', '865522888', 'nemunas@irankiunuoma.lt', 2, 2),
('Raudone', 'raudondvario 2', '86665432', 'raudone@irankiunuoma.lt', 3, 3),
('Merkine', 'Merkines 7', '854439394', 'merkine@irankiunuoma.lt', 4, 4),
('Brasta', 'Brastos 12', '86542244', 'brasta@irankiunuoma.lt', 5, 5);

-- --------------------------------------------------------

--
-- Table structure for table `nuomos_tarifas`
--

CREATE TABLE `nuomos_tarifas` (
  `tarifas` float DEFAULT NULL,
  `galioja_nuo` date NOT NULL DEFAULT '0000-00-00',
  `galioja_iki` date DEFAULT NULL,
  `fk_IRANKIU_GRUPEid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci;

--
-- Dumping data for table `nuomos_tarifas`
--

INSERT INTO `nuomos_tarifas` (`tarifas`, `galioja_nuo`, `galioja_iki`, `fk_IRANKIU_GRUPEid`) VALUES
(10, '2016-03-02', '2017-05-05', 1),
(15, '2016-03-03', '2017-06-05', 2),
(20, '2016-04-02', '2017-07-05', 3),
(25, '2016-05-03', '2017-08-05', 4);

-- --------------------------------------------------------

--
-- Table structure for table `papildomas_mokestis`
--

CREATE TABLE `papildomas_mokestis` (
  `kaina` float DEFAULT NULL,
  `kiekis` int(11) DEFAULT NULL,
  `id` int(11) NOT NULL DEFAULT '0',
  `fk_MOKESTISid` int(11) NOT NULL,
  `fk_SUTARTISnr` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci;

--
-- Dumping data for table `papildomas_mokestis`
--

INSERT INTO `papildomas_mokestis` (`kaina`, `kiekis`, `id`, `fk_MOKESTISid`, `fk_SUTARTISnr`) VALUES
(384, 2, 1, 3, 1),
(1304, 14, 2, 4, 2),
(358, 13, 3, 3, 3),
(1164, 14, 4, 3, 4),
(785, 8, 5, 4, 5),
(879, 3, 6, 4, 6),
(1000, 19, 7, 3, 7),
(1597, 9, 8, 4, 8),
(1355, 2, 9, 4, 9),
(968, 18, 10, 4, 10),
(1942, 16, 11, 2, 11),
(1075, 15, 12, 1, 12),
(953, 17, 13, 1, 13),
(1330, 5, 14, 1, 14),
(1449, 7, 15, 1, 15),
(650, 19, 16, 2, 16),
(381, 12, 17, 3, 17),
(413, 16, 18, 4, 18),
(1761, 7, 19, 2, 19),
(335, 12, 20, 4, 20),
(1852, 2, 21, 1, 21),
(1455, 3, 22, 1, 22),
(1312, 4, 23, 2, 23),
(1052, 4, 24, 4, 24),
(867, 17, 25, 1, 25),
(840, 3, 26, 4, 26),
(1014, 16, 27, 2, 27),
(289, 7, 28, 3, 28),
(189, 10, 29, 4, 29),
(1527, 20, 30, 2, 30);

-- --------------------------------------------------------

--
-- Table structure for table `paslauga`
--

CREATE TABLE `paslauga` (
  `pavadinimas` varchar(255) COLLATE utf8_lithuanian_ci DEFAULT NULL,
  `aprasymas` varchar(255) COLLATE utf8_lithuanian_ci DEFAULT NULL,
  `id` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci;

--
-- Dumping data for table `paslauga`
--

INSERT INTO `paslauga` (`pavadinimas`, `aprasymas`, `id`) VALUES
('Specialisto pagalba', 'Apmokyto profesionalo pagalba darbuose', 1),
('Irankio naudojimo instruktazas', 'Specialus kursai skirti aprodyti iranki', 2),
('Irankio remontas', 'Jusu irankio remonto darbai, prieziura', 3),
('Irankio pervezimas', 'Pervesime jusu iranki I norima vieta', 4),
('Apsaugines aprangos komplektas', 'Isnuomosime Jums specialu aprangos komplekta', 5);

-- --------------------------------------------------------

--
-- Table structure for table `paslaugos_kaina`
--

CREATE TABLE `paslaugos_kaina` (
  `galioja_nuo` date NOT NULL DEFAULT '0000-00-00',
  `galioja_iki` date DEFAULT NULL,
  `kaina` float DEFAULT NULL,
  `fk_PASLAUGAid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci;

--
-- Dumping data for table `paslaugos_kaina`
--

INSERT INTO `paslaugos_kaina` (`galioja_nuo`, `galioja_iki`, `kaina`, `fk_PASLAUGAid`) VALUES
('2016-02-03', '2017-07-03', 299, 1),
('2016-02-04', '2017-07-04', 199, 2),
('2016-02-05', '2017-05-05', 39, 3),
('2016-05-06', '2017-05-06', 59, 5),
('2016-06-05', '2017-06-05', 49, 4);

-- --------------------------------------------------------

--
-- Table structure for table `sandelis`
--

CREATE TABLE `sandelis` (
  `pavadinimas` varchar(255) COLLATE utf8_lithuanian_ci DEFAULT NULL,
  `adresas` varchar(255) COLLATE utf8_lithuanian_ci DEFAULT NULL,
  `id` int(11) NOT NULL DEFAULT '0',
  `fk_MIESTASid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci;

--
-- Dumping data for table `sandelis`
--

INSERT INTO `sandelis` (`pavadinimas`, `adresas`, `id`, `fk_MIESTASid`) VALUES
('Sandelys nr. 1', 'Partizanu 2', 1, 1),
('Sandelis nr. 2', 'Brastos 5', 2, 1),
('Sandelis nr. 3', 'Karmelitu 11', 3, 2),
('Sandelis nr. 4', 'Kreves 12', 4, 2),
('Sandelis nr. 5', 'Sargenu 16', 5, 3),
('Sandelis nr. 6', 'Juodeliu 3', 6, 3),
('Sandelis nr. 7', 'Partizanu 11', 7, 4),
('Sandelis nr. 8', 'Gebeniu 4', 8, 4),
('Sandelis nr. 9', 'Braskiu 2', 9, 5),
('Sandelis nr. 10', 'Melyniu 34', 10, 5);

-- --------------------------------------------------------

--
-- Table structure for table `saskaita`
--

CREATE TABLE `saskaita` (
  `nr` int(11) NOT NULL DEFAULT '0',
  `data` date DEFAULT NULL,
  `suma` float DEFAULT NULL,
  `fk_SUTARTISnr` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci;

--
-- Dumping data for table `saskaita`
--

INSERT INTO `saskaita` (`nr`, `data`, `suma`, `fk_SUTARTISnr`) VALUES
(1, '0000-00-00', 1696, 1),
(2, '0000-00-00', 730, 2),
(3, '0000-00-00', 731, 3),
(4, '0000-00-00', 1555, 4),
(5, '0000-00-00', 1676, 5),
(6, '0000-00-00', 677, 6),
(7, '0000-00-00', 2368, 7),
(8, '0000-00-00', 1932, 8),
(9, '0000-00-00', 394, 9),
(10, '0000-00-00', 1597, 10),
(11, '0000-00-00', 2102, 11),
(12, '0000-00-00', 260, 12),
(13, '0000-00-00', 1369, 13),
(14, '0000-00-00', 1997, 14),
(15, '0000-00-00', 1996, 15),
(16, '0000-00-00', 1810, 16),
(17, '0000-00-00', 2034, 17),
(18, '0000-00-00', 696, 18),
(19, '0000-00-00', 922, 19),
(20, '0000-00-00', 2151, 20),
(21, '0000-00-00', 516, 21),
(22, '0000-00-00', 336, 22),
(23, '0000-00-00', 2284, 23),
(24, '0000-00-00', 2167, 24),
(25, '0000-00-00', 1531, 25),
(26, '0000-00-00', 1350, 26),
(27, '0000-00-00', 2062, 27),
(28, '0000-00-00', 1687, 28),
(29, '0000-00-00', 708, 29),
(30, '0000-00-00', 344, 30);

-- --------------------------------------------------------

--
-- Table structure for table `sutartis`
--

CREATE TABLE `sutartis` (
  `nr` int(11) NOT NULL DEFAULT '0',
  `sutarties_data` date DEFAULT NULL,
  `nuomos_data` date DEFAULT NULL,
  `planuojama_grazinimo_data` date DEFAULT NULL,
  `faktine_grazinimo_data` date DEFAULT NULL,
  `busena` varchar(255) COLLATE utf8_lithuanian_ci DEFAULT NULL,
  `pradine_bukle` int(11) DEFAULT NULL,
  `galutine_bukle` int(11) DEFAULT NULL,
  `kaina` float DEFAULT NULL,
  `fk_IRANKISid` int(11) NOT NULL,
  `fk_DARBUOTOJAStabelio_nr` int(11) NOT NULL,
  `fk_KLIENTASasmens_kodas` varchar(255) COLLATE utf8_lithuanian_ci NOT NULL,
  `fk_NUOMOS_TARIFASgalioja_nuo` date NOT NULL,
  `fk_SANDELISid` int(11) NOT NULL,
  `fk_SANDELISid1` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci;

--
-- Dumping data for table `sutartis`
--

INSERT INTO `sutartis` (`nr`, `sutarties_data`, `nuomos_data`, `planuojama_grazinimo_data`, `faktine_grazinimo_data`, `busena`, `pradine_bukle`, `galutine_bukle`, `kaina`, `fk_IRANKISid`, `fk_DARBUOTOJAStabelio_nr`, `fk_KLIENTASasmens_kodas`, `fk_NUOMOS_TARIFASgalioja_nuo`, `fk_SANDELISid`, `fk_SANDELISid1`) VALUES
(1, '2016-08-02', '2016-08-20', '2016-08-26', '0000-00-00', 'isnuomota', 6, 6, 1546, 5, 15, '39410242897', '2016-03-03', 2, 1),
(2, '2017-01-07', '2017-01-25', '2017-02-12', '2017-02-13', 'baigta', 7, 7, 580, 3, 8, '45311163838', '2016-04-02', 1, 7),
(3, '2016-11-12', '2016-11-16', '2016-11-24', '2016-12-08', 'uzbaigta', 8, 8, 581, 6, 24, '47012259078', '2016-03-03', 6, 10),
(4, '2017-02-23', '2017-03-08', '2017-03-22', '2017-04-07', 'patvirtinta', 4, 4, 1405, 8, 30, '46912317146', '2016-04-02', 6, 10),
(5, '2016-12-10', '2016-12-22', '2017-01-02', '2017-01-14', 'uzbaigta', 5, 4, 1526, 3, 30, '49012158209', '2016-03-02', 5, 7),
(6, '2016-12-31', '2017-01-14', '2017-01-25', '2017-02-03', 'nutraukta', 7, 7, 527, 10, 5, '35711166049', '2016-03-03', 4, 7),
(7, '2016-08-20', '2016-08-30', '2016-09-05', '2016-09-07', 'nutraukta', 8, 7, 2218, 15, 9, '39511136936', '2016-05-03', 9, 3),
(8, '2017-01-24', '2017-02-07', '2017-02-14', '2017-02-19', 'baigta', 8, 8, 1782, 60, 15, '37411268910', '2016-04-02', 6, 10),
(9, '2016-11-19', '2016-11-25', '2016-11-30', '2016-12-19', 'baigta', 4, 2, 244, 45, 29, '38612123623', '2016-03-02', 6, 9),
(10, '2017-01-30', '2017-01-31', '2017-02-14', '2017-03-05', 'baigta', 6, 6, 1447, 33, 8, '49510161936', '2016-05-03', 7, 4),
(11, '2016-10-20', '2016-10-28', '2016-11-15', '2016-12-02', 'baigta', 5, 3, 1952, 64, 18, '46412151151', '2016-03-03', 7, 8),
(12, '2017-03-18', '2017-04-02', '2017-04-16', '2017-04-30', 'baigta', 4, 2, 110, 24, 26, '36110283145', '2016-04-02', 4, 7),
(13, '2017-01-21', '2017-02-05', '2017-02-15', '2017-02-24', 'baigta', 5, 3, 1219, 77, 14, '35311152794', '2016-05-03', 5, 9),
(14, '2016-10-12', '2016-10-17', '2016-11-04', '2016-11-21', 'baigta', 9, 7, 1847, 63, 17, '38310198416', '2016-03-03', 10, 8),
(15, '2016-08-09', '2016-08-23', '2016-09-04', '2016-09-17', 'baigta', 9, 8, 1846, 12, 7, '49311243575', '2016-05-03', 4, 10),
(16, '2016-10-12', '2016-10-23', '2016-10-29', '2016-11-15', 'baigta', 6, 6, 1660, 41, 1, '35011229196', '2016-03-03', 5, 5),
(17, '2016-08-20', '2016-08-24', '2016-09-09', '2016-09-08', 'baigta', 6, 4, 1884, 61, 5, '38111115907', '2016-05-03', 5, 4),
(18, '2016-08-28', '2016-09-09', '2016-09-21', '2016-09-20', 'baigta', 7, 6, 546, 71, 9, '45511288324', '2016-04-02', 4, 9),
(19, '2016-12-10', '2016-12-25', '2017-01-05', '2017-01-25', 'baigta', 4, 2, 772, 81, 11, '49010106372', '2016-05-03', 8, 1),
(20, '2017-01-19', '2017-02-01', '2017-02-16', '2017-02-17', 'baigta', 8, 7, 2001, 79, 26, '37010218997', '2016-03-03', 9, 5),
(21, '2016-09-15', '2016-09-29', '2016-10-16', '2016-10-30', 'baigta', 8, 8, 366, 97, 12, '46510186409', '2016-04-02', 5, 5),
(22, '2016-10-25', '2016-11-13', '2016-11-16', '2016-12-05', 'baigta', 10, 9, 186, 54, 28, '47812132200', '2016-03-03', 6, 5),
(23, '2017-03-12', '2017-03-30', '2017-04-01', '2017-04-14', 'uzsakyta', 5, 3, 2134, 18, 13, '38112304369', '2016-04-02', 8, 9),
(24, '2017-03-19', '2017-03-25', '2017-04-09', '2017-04-13', 'patvirtinta', 4, 2, 2017, 19, 28, '36712148325', '2016-03-02', 3, 8),
(25, '2016-10-08', '2016-10-12', '2016-11-01', '2016-11-14', 'nutraukta', 7, 6, 1381, 20, 29, '39411312645', '2016-03-03', 3, 7),
(26, '2016-09-13', '2016-09-17', '2016-10-01', '2016-10-01', 'nutraukta', 4, 4, 1200, 46, 4, '36410313397', '2016-04-02', 8, 3),
(27, '2016-09-21', '2016-10-07', '2016-10-18', '2016-11-03', 'nutraukta', 5, 3, 1912, 57, 6, '48712248881', '2016-03-02', 6, 4),
(28, '2017-02-26', '2017-03-16', '2017-04-01', '2017-04-15', 'nutraukta', 9, 7, 1537, 42, 17, '46211298826', '2016-03-03', 6, 8),
(29, '2016-07-14', '2016-07-20', '2016-08-09', '2016-08-22', 'nutraukta', 10, 9, 558, 24, 25, '38612159346', '2016-03-02', 3, 9),
(30, '2017-03-24', '2017-04-05', '2017-04-17', '2017-04-27', 'nutraukta', 4, 3, 194, 51, 23, '35111132868', '2016-04-02', 7, 10),
(33, '2016-01-26', '2016-01-23', '2016-01-29', '2016-01-31', 'baigta', 10, 8, 300, 2, 4, '35311152794', '2016-03-03', 4, 1),
(34, '2016-01-26', '2016-01-23', '2016-01-29', '2016-01-31', 'baigta', 10, 8, 300, 2, 4, '35311152794', '2016-03-03', 4, 1),
(35, '2016-01-27', '2016-01-29', '2016-01-30', '2016-01-31', 'baigta', 10, 9, 300, 17, 9, '36011116518', '2016-03-03', 6, 8),
(36, '2016-01-01', '2017-01-23', '2017-01-25', '2017-01-29', 'paimta', 8, 7, 200, 93, 3, '35111132868', '2016-03-03', 5, 4);

-- --------------------------------------------------------

--
-- Table structure for table `uzsakyta_paslauga`
--

CREATE TABLE `uzsakyta_paslauga` (
  `kiekis` int(11) DEFAULT NULL,
  `kaina` float DEFAULT NULL,
  `id` int(11) NOT NULL DEFAULT '0',
  `fk_SUTARTISnr` int(11) NOT NULL,
  `fk_PASLAUGOS_KAINAgalioja_nuo` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci;

--
-- Dumping data for table `uzsakyta_paslauga`
--

INSERT INTO `uzsakyta_paslauga` (`kiekis`, `kaina`, `id`, `fk_SUTARTISnr`, `fk_PASLAUGOS_KAINAgalioja_nuo`) VALUES
(1, 299, 0, 35, '2016-02-03'),
(14, 1025, 2, 2, '2016-02-04'),
(43, 1037, 3, 3, '2016-02-05'),
(35, 3418, 4, 4, '2016-06-05'),
(23, 2336, 5, 5, '2016-05-06'),
(19, 6056, 7, 2, '2016-02-04'),
(19, 4083, 8, 3, '2016-02-05'),
(42, 1235, 9, 4, '2016-06-05'),
(25, 2070, 10, 5, '2016-05-06'),
(1, 5120, 12, 2, '2016-02-04'),
(25, 9093, 13, 3, '2016-02-05'),
(46, 7467, 14, 4, '2016-06-05'),
(47, 3432, 15, 5, '2016-05-06'),
(13, 5942, 17, 2, '2016-02-04'),
(21, 1532, 18, 3, '2016-02-05'),
(46, 4602, 19, 4, '2016-06-05'),
(47, 1302, 20, 5, '2016-05-06'),
(42, 3897, 22, 2, '2016-02-04'),
(44, 8513, 23, 3, '2016-02-05'),
(40, 6245, 24, 4, '2016-06-05'),
(13, 7042, 25, 5, '2016-05-06'),
(42, 1882, 27, 2, '2016-02-04'),
(26, 4323, 28, 3, '2016-02-05'),
(43, 6201, 29, 4, '2016-06-05'),
(44, 1062, 30, 5, '2016-05-06'),
(1, 300, 105, 33, '2016-02-03');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `aptarnauja`
--
ALTER TABLE `aptarnauja`
  ADD PRIMARY KEY (`fk_SANDELISid`,`fk_DARBUOTOJAStabelio_nr`);

--
-- Indexes for table `darbuotojas`
--
ALTER TABLE `darbuotojas`
  ADD PRIMARY KEY (`tabelio_nr`),
  ADD KEY `dirba` (`fk_NUOMOS_BIURASid`),
  ADD KEY `fk_NAUDOTOJASid` (`fk_NAUDOTOJASid`);

--
-- Indexes for table `firma`
--
ALTER TABLE `firma`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `irankis`
--
ALTER TABLE `irankis`
  ADD PRIMARY KEY (`id`),
  ADD KEY `turi1` (`fk_MODELISid`),
  ADD KEY `priklauso` (`fk_IRANKIU_GRUPEid`);

--
-- Indexes for table `irankiu_grupe`
--
ALTER TABLE `irankiu_grupe`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `klientas`
--
ALTER TABLE `klientas`
  ADD PRIMARY KEY (`asmens_kodas`),
  ADD KEY `fk_NAUDOTOJASid` (`fk_NAUDOTOJASid`);

--
-- Indexes for table `miestas`
--
ALTER TABLE `miestas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `modelis`
--
ALTER TABLE `modelis`
  ADD PRIMARY KEY (`id`),
  ADD KEY `turi` (`fk_FIRMAid`);

--
-- Indexes for table `mokejimas`
--
ALTER TABLE `mokejimas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sumokejo` (`fk_KLIENTASasmens_kodas`),
  ADD KEY `apmoka` (`fk_SASKAITAnr`);

--
-- Indexes for table `mokestis`
--
ALTER TABLE `mokestis`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `naudotojas`
--
ALTER TABLE `naudotojas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_NAUDOTOJO_BUSENAid` (`fk_NAUDOTOJO_BUSENAid`),
  ADD KEY `fk_NAUDOTOJO_ROLEid` (`fk_NAUDOTOJO_ROLEid`);

--
-- Indexes for table `naudotojo_busena`
--
ALTER TABLE `naudotojo_busena`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `naudotojo_role`
--
ALTER TABLE `naudotojo_role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nuomos_biuras`
--
ALTER TABLE `nuomos_biuras`
  ADD PRIMARY KEY (`id`),
  ADD KEY `turi3` (`fk_MIESTASid`);

--
-- Indexes for table `nuomos_tarifas`
--
ALTER TABLE `nuomos_tarifas`
  ADD PRIMARY KEY (`galioja_nuo`),
  ADD KEY `nuomuojama_uz` (`fk_IRANKIU_GRUPEid`);

--
-- Indexes for table `papildomas_mokestis`
--
ALTER TABLE `papildomas_mokestis`
  ADD PRIMARY KEY (`id`),
  ADD KEY `priskaiciuota` (`fk_MOKESTISid`),
  ADD KEY `priskirtas` (`fk_SUTARTISnr`);

--
-- Indexes for table `paslauga`
--
ALTER TABLE `paslauga`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `paslaugos_kaina`
--
ALTER TABLE `paslaugos_kaina`
  ADD PRIMARY KEY (`galioja_nuo`),
  ADD KEY `teikiama_uz` (`fk_PASLAUGAid`);

--
-- Indexes for table `sandelis`
--
ALTER TABLE `sandelis`
  ADD PRIMARY KEY (`id`),
  ADD KEY `turi2` (`fk_MIESTASid`);

--
-- Indexes for table `saskaita`
--
ALTER TABLE `saskaita`
  ADD PRIMARY KEY (`nr`),
  ADD KEY `israsyta` (`fk_SUTARTISnr`);

--
-- Indexes for table `sutartis`
--
ALTER TABLE `sutartis`
  ADD PRIMARY KEY (`nr`),
  ADD KEY `itrauktas` (`fk_IRANKISid`),
  ADD KEY `patvirtina` (`fk_DARBUOTOJAStabelio_nr`),
  ADD KEY `sudaro` (`fk_KLIENTASasmens_kodas`),
  ADD KEY `taikomas` (`fk_NUOMOS_TARIFASgalioja_nuo`),
  ADD KEY `nurodyta2` (`fk_SANDELISid`),
  ADD KEY `nurodyta` (`fk_SANDELISid1`);

--
-- Indexes for table `uzsakyta_paslauga`
--
ALTER TABLE `uzsakyta_paslauga`
  ADD PRIMARY KEY (`id`),
  ADD KEY `itraukta` (`fk_SUTARTISnr`),
  ADD KEY `teikiama` (`fk_PASLAUGOS_KAINAgalioja_nuo`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `naudotojas`
--
ALTER TABLE `naudotojas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=147;
--
-- AUTO_INCREMENT for table `naudotojo_busena`
--
ALTER TABLE `naudotojo_busena`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `naudotojo_role`
--
ALTER TABLE `naudotojo_role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `aptarnauja`
--
ALTER TABLE `aptarnauja`
  ADD CONSTRAINT `aptarnauja` FOREIGN KEY (`fk_SANDELISid`) REFERENCES `sandelis` (`id`);

--
-- Constraints for table `darbuotojas`
--
ALTER TABLE `darbuotojas`
  ADD CONSTRAINT `darbuotojas_ibfk_1` FOREIGN KEY (`fk_NAUDOTOJASid`) REFERENCES `naudotojas` (`id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `dirba` FOREIGN KEY (`fk_NUOMOS_BIURASid`) REFERENCES `nuomos_biuras` (`id`);

--
-- Constraints for table `irankis`
--
ALTER TABLE `irankis`
  ADD CONSTRAINT `priklauso` FOREIGN KEY (`fk_IRANKIU_GRUPEid`) REFERENCES `irankiu_grupe` (`id`),
  ADD CONSTRAINT `turi1` FOREIGN KEY (`fk_MODELISid`) REFERENCES `modelis` (`id`);

--
-- Constraints for table `klientas`
--
ALTER TABLE `klientas`
  ADD CONSTRAINT `klientas_ibfk_1` FOREIGN KEY (`fk_NAUDOTOJASid`) REFERENCES `naudotojas` (`id`) ON UPDATE NO ACTION;

--
-- Constraints for table `modelis`
--
ALTER TABLE `modelis`
  ADD CONSTRAINT `turi` FOREIGN KEY (`fk_FIRMAid`) REFERENCES `firma` (`id`);

--
-- Constraints for table `mokejimas`
--
ALTER TABLE `mokejimas`
  ADD CONSTRAINT `apmoka` FOREIGN KEY (`fk_SASKAITAnr`) REFERENCES `saskaita` (`nr`),
  ADD CONSTRAINT `sumokejo` FOREIGN KEY (`fk_KLIENTASasmens_kodas`) REFERENCES `klientas` (`asmens_kodas`);

--
-- Constraints for table `naudotojas`
--
ALTER TABLE `naudotojas`
  ADD CONSTRAINT `naudotojas_ibfk_1` FOREIGN KEY (`fk_NAUDOTOJO_ROLEid`) REFERENCES `naudotojo_role` (`id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `naudotojas_ibfk_2` FOREIGN KEY (`fk_NAUDOTOJO_BUSENAid`) REFERENCES `naudotojo_busena` (`id`) ON UPDATE NO ACTION;

--
-- Constraints for table `nuomos_biuras`
--
ALTER TABLE `nuomos_biuras`
  ADD CONSTRAINT `turi3` FOREIGN KEY (`fk_MIESTASid`) REFERENCES `miestas` (`id`);

--
-- Constraints for table `nuomos_tarifas`
--
ALTER TABLE `nuomos_tarifas`
  ADD CONSTRAINT `nuomuojama_uz` FOREIGN KEY (`fk_IRANKIU_GRUPEid`) REFERENCES `irankiu_grupe` (`id`);

--
-- Constraints for table `papildomas_mokestis`
--
ALTER TABLE `papildomas_mokestis`
  ADD CONSTRAINT `priskaiciuota` FOREIGN KEY (`fk_MOKESTISid`) REFERENCES `mokestis` (`id`),
  ADD CONSTRAINT `priskirtas` FOREIGN KEY (`fk_SUTARTISnr`) REFERENCES `sutartis` (`nr`);

--
-- Constraints for table `paslaugos_kaina`
--
ALTER TABLE `paslaugos_kaina`
  ADD CONSTRAINT `teikiama_uz` FOREIGN KEY (`fk_PASLAUGAid`) REFERENCES `paslauga` (`id`);

--
-- Constraints for table `sandelis`
--
ALTER TABLE `sandelis`
  ADD CONSTRAINT `turi2` FOREIGN KEY (`fk_MIESTASid`) REFERENCES `miestas` (`id`);

--
-- Constraints for table `saskaita`
--
ALTER TABLE `saskaita`
  ADD CONSTRAINT `israsyta` FOREIGN KEY (`fk_SUTARTISnr`) REFERENCES `sutartis` (`nr`);

--
-- Constraints for table `sutartis`
--
ALTER TABLE `sutartis`
  ADD CONSTRAINT `itrauktas` FOREIGN KEY (`fk_IRANKISid`) REFERENCES `irankis` (`id`),
  ADD CONSTRAINT `nurodyta` FOREIGN KEY (`fk_SANDELISid1`) REFERENCES `sandelis` (`id`),
  ADD CONSTRAINT `nurodyta2` FOREIGN KEY (`fk_SANDELISid`) REFERENCES `sandelis` (`id`),
  ADD CONSTRAINT `patvirtina` FOREIGN KEY (`fk_DARBUOTOJAStabelio_nr`) REFERENCES `darbuotojas` (`tabelio_nr`),
  ADD CONSTRAINT `sudaro` FOREIGN KEY (`fk_KLIENTASasmens_kodas`) REFERENCES `klientas` (`asmens_kodas`),
  ADD CONSTRAINT `taikomas` FOREIGN KEY (`fk_NUOMOS_TARIFASgalioja_nuo`) REFERENCES `nuomos_tarifas` (`galioja_nuo`);

--
-- Constraints for table `uzsakyta_paslauga`
--
ALTER TABLE `uzsakyta_paslauga`
  ADD CONSTRAINT `itraukta` FOREIGN KEY (`fk_SUTARTISnr`) REFERENCES `sutartis` (`nr`),
  ADD CONSTRAINT `teikiama` FOREIGN KEY (`fk_PASLAUGOS_KAINAgalioja_nuo`) REFERENCES `paslaugos_kaina` (`galioja_nuo`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
