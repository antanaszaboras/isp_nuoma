<?php

/*
 * accessLevels:
 * 0 - client
 * 1 - worker
 * 2 - admin
 */
function checkAccess($accessLevel){
    switch($accessLevel){
        case 0:
            return true;
        break;
        case 1:
            if($_SESSION['darbuotojas'])
                return true;
        break;
        case 2:
            if($_SESSION['administratorius'])
                return true;
        break;
    }
        
        
    return false;
}
