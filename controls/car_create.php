<?php

include 'libraries/cars.class.php';
$carsObj = new cars();

include 'libraries/brands.class.php';
$brandsObj = new brands();

include 'libraries/models.class.php';
$modelsObj = new models();

$formErrors = null;
$data = array();

// nustatome privalomus laukus
$required = array('id', 'fk_MODELISid', 'pagaminimo_data', 'greicio_tipai', 'kuro_tipas', 'galingumas', 'bagazo_dydis', 'busena', 'dydis', 'svoris', 'fk_IRANKIU_GRUPEid', 'busena');

// maksimalūs leidžiami laukų ilgiai
$maxLengths = array (
	'id' => 5,
	'valstybinis_nr' => 6
);

// vartotojas paspaudė išsaugojimo mygtuką
if(!empty($_POST['submit'])) {
	// nustatome laukų validatorių tipus
	$validations = array (
		'id' => 'alfanum',
		'fk_MODELISid' => 'anything',
		'greicio_tipai' => 'anything',
		'kuro_tipas' => 'anything',
		'pagaminimo_data' => 'anything',
		'irankio_tipas' => 'anything',
		'galingumas' => 'positivenumber',
		'dydis' => 'anything',
		'busena' => 'anything',
		'svoris' => 'positivenumber',
		'fk_IRANKIU_GRUPEid' => 'anything'
		);

	// sukuriame laukų validatoriaus objektą
	include 'utils/validator.class.php';
	$validator = new validator($validations, $required, $maxLengths);

	// laukai įvesti be klaidų
	if($validator->validate($_POST)) {
		// suformuojame laukų reikšmių masyvą SQL užklausai
		$dataPrepared = $validator->preparePostFieldsForSQL();

		// įrašome naują įrašą
		$carsObj->insertCar($dataPrepared);

		// nukreipiame vartotoją į automobilių puslapį
		header("Location: index.php?module={$module}&action=list");
		die();
	} else {
		// gauname klaidų pranešimą
		$formErrors = $validator->getErrorHTML();
		// laukų reikšmių kintamajam priskiriame įvestų laukų reikšmes
		$data = $_POST;
	}
} else {
	// tikriname, ar nurodytas elemento id. Jeigu taip, išrenkame elemento duomenis ir jais užpildome formos laukus.
	if(!empty($id)) {
		// išrenkame automobilį
		$data = $carsObj->getCar($id);
	}
}

// įtraukiame šabloną
include 'templates/car_form.tpl.php';

?>