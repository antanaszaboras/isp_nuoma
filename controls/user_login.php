<?php
	
include 'libraries/users.class.php';
$usersObj = new users();

$formErrors = null;
$data = array();
$maxLengths = array (
	'el_pastas' => 200,
	'slaptazodis' => 200
);
// nustatome privalomus formos laukus
$required = array('el_pastas', 'slaptazodis');

// vartotojas paspaudė išsaugojimo mygtuką
if(!empty($_POST['submit'])) {
	include 'utils/validator.class.php';

	// nustatome laukų validatorių tipus
	$validations = array (
		'el_pastas' => 'email',
		'slaptazodis' => 'not_empty'
	);

	// sukuriame laukų validatoriaus objektą
	$validator = new validator($validations, $required, $maxLengths);

	// laukai įvesti be klaidų
	if($validator->validate($_POST)) {
		// suformuojame laukų reikšmių masyvą SQL užklausai
		$dataPrepared = $validator->preparePostFieldsForSQL();

		// redaguojame klientą
		//print_r($dataPrepared);
		$result = $usersObj->checkUsernamePassword($dataPrepared['el_pastas'],$dataPrepared['slaptazodis']);
		if($result > 0){
			$userData = $usersObj->getUser($result);
			foreach($userData as $key => $value){
				$_SESSION[$key] = $value;
			}
			header("Location: index.php");
			
		}else{
			if($result == 0)
				$formErrors = "KLAIDA<br/><ul>" . '<li> Prisijungimas draudžiamas. </li>' . "</ul>";
			if($result == -1)
				$formErrors = "KLAIDA<br/><ul>" . '<li> Blogas prisijungimo vardas arba slaptažodis. </li>' . "</ul>";
    	}
		
		

		// nukreipiame vartotoją į klientų puslapį
		//header("Location: index.php");
		//die();
	}
	else {
		// gauname klaidų pranešimą
		$formErrors = $validator->getErrorHTML();
		// laukų reikšmių kintamajam priskiriame įvestų laukų reikšmes
		$data = $_POST;
	}
}

// įtraukiame šabloną
include 'templates/login_form.tpl.php';

?>