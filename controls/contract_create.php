<?php
	
include 'libraries/contracts.class.php';
$contractsObj = new contracts();

include 'libraries/services.class.php';
$servicesObj = new services();

include 'libraries/cars.class.php';
$carsObj = new cars();

include 'libraries/employees.class.php';
$employeesObj = new employees();

include 'libraries/customers.class.php';
$customersObj = new customers();

$formErrors = null;
$data = array();

// nustatome privalomus laukus
$required = array('nr', 'sutarties_data', 'nuomos_data', 'planuojama_grazinimo_data', 'pradine_bukle', 'galutine_bukle', 'kaina', 'busena', 'fk_KLIENTASasmens_kodas', 'fk_DARBUOTOJAStabelio_nr', 'fk_IRANKISid', 'fk_SANDELISid', 'fk_SANDELISid1', 'fk_NUOMOS_TARIFASgalioja_nuo');

// vartotojas paspaudė išsaugojimo mygtuką
if(!empty($_POST['submit'])) {
	include 'utils/validator.class.php';

	// nustatome laukų validatorių tipus
	$validations = array (
		'nr' => 'positivenumber',
		'sutarties_data' => 'date',
		'nuomos_data' => 'date',
		'planuojama_grazinimo_data' => 'date',
		'faktine_grazinimo_data' => 'date',
		'pradine_bukle' => 'int',
		'galutine_bukle' => 'int',
		'kaina' => 'price',
		'busena' => 'anything',
		'fk_KLIENTASasmens_kodas' => 'alfanum',
		'fk_DARBUOTOJAStabelio_nr' => 'alfanum',
		'fk_IRANKISid' => 'positivenumber',
		'fk_SANDELISid' => 'positivenumber',
		'fk_SANDELISid1' => 'positivenumber',
		'fk_NUOMOS_TARIFASgalioja_nuo' => 'anything'
		);


	// sukuriame laukų validatoriaus objektą
	$validator = new validator($validations, $required);

	// laukai įvesti be klaidų
	if($validator->validate($_POST)) {
		// suformuojame laukų reikšmių masyvą SQL užklausai
		$dataPrepared = $validator->preparePostFieldsForSQL();

		// patikriname, ar nėra sutarčių su tokiu pačiu numeriu
		$tmp = $contractsObj->getContract($dataPrepared['nr']);

		if(isset($tmp['nr'])) {
			// sudarome klaidų pranešimą
			$formErrors = "Sutartis su įvestu numeriu jau egzistuoja.";
			// laukų reikšmių kintamajam priskiriame įvestų laukų reikšmes
			$data = $_POST;
		} else {
			// įrašome naują sutartį
			$contractsObj->insertContract($dataPrepared);

			// įrašome užsakytas paslaugas
			$contractsObj->updateOrderedServices($dataPrepared);
		}
		
		// nukreipiame vartotoją į sutarčių puslapį
		if($formErrors == null) {
			header("Location: index.php?module={$module}&action=list");
			die();
		}
	} else {
		// gauname klaidų pranešimą
		$formErrors = $validator->getErrorHTML();

		// laukų reikšmių kintamajam priskiriame įvestų laukų reikšmes
		$data = $_POST;
		if(isset($_POST['kiekiai']) && sizeof($_POST['kiekiai']) > 0) {
			$i = 0;
			foreach($_POST['kiekiai'] as $key => $val) {
				$data['uzsakytos_paslaugos'][$i]['kiekis'] = $val;
				$i++;
			}
		}
	}
}

// įtraukiame šabloną
include 'templates/contract_form.tpl.php';

?>