<?php

include 'libraries/cars.class.php';
$carsObj = new cars();

include 'libraries/brands.class.php';
$brandsObj = new brands();

include 'libraries/models.class.php';
$modelsObj = new models();

$formErrors = null;
$data = array();

// nustatome privalomus laukus
$required = array('id', 'fk_MODELISid', 'greicio_tipai', 'pagaminimo_data','kuro_tipas', 'irankio_tipas', 'galingumas', 'dydis', 'busena', 'svoris', 'fk_IRANKIU_GRUPEid');

// maksimalūs leidžiami laukų ilgiai
$maxLengths = array (
	'id' => 6,
	'fk_MODELISid' => 6
);

// vartotojas paspaudė išsaugojimo mygtuką
if(!empty($_POST['submit'])) {
	// nustatome laukų validatorių tipus
	$validations = array (
		'id' => 'alfanum',
		'fk_MODELISid' => 'positivenumber',
		'greicio_tipai' => 'anything',
		'kuro_tipas' => 'anything',
		'pagaminimo_data' => 'anything',
		'irankio_tipas' => 'anything',
		'galingumas' => 'positivenumber',
		'dydis' => 'anything',
		'busena' => 'anything',
		'svoris' => 'positivenumber',
		'fk_IRANKIU_GRUPEid' => 'positivenumber'
		);

	// sukuriame laukų validatoriaus objektą
	include 'utils/validator.class.php';
	$validator = new validator($validations, $required, $maxLengths);

	// laukai įvesti be klaidų
	if($validator->validate($_POST)) {
		// suformuojame laukų reikšmių masyvą SQL užklausai
		$dataPrepared = $validator->preparePostFieldsForSQL();

		

		// atnaujiname duomenis
		$carsObj->updateCar($dataPrepared);

		// nukreipiame vartotoją į automobilių puslapį
		header("Location: index.php?module={$module}&action=list");
		die();
	} else {
		// gauname klaidų pranešimą
		$formErrors = $validator->getErrorHTML();
		// laukų reikšmių kintamajam priskiriame įvestų laukų reikšmes
		$data = $_POST;
	}
} else {
	// išrenkame elemento duomenis ir jais užpildome formos laukus.
	$data = $carsObj->getCar($id);
}

// įtraukiame šabloną
include 'templates/car_form.tpl.php';

?>