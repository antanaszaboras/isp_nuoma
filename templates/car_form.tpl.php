<ul id="pagePath">
	<li><a href="index.php">Pradžia</a></li>
	<li><a href="index.php?module=<?php echo $module; ?>&action=list">Įrankiai</a></li>
	<li><?php if(!empty($id)) echo "Įrankio redagavimas"; else echo "Naujas įrankis"; ?></li>
</ul>
<div class="float-clear"></div>
<div id="formContainer">
	<?php if($formErrors != null) { ?>
		<div class="errorBox">
			Neįvesti arba neteisingai įvesti šie laukai:
			<?php 
				echo $formErrors;
			?>
		</div>
	<?php } ?>
	<form action="" method="post">
		<fieldset>
			<legend>Įrankio informacija</legend>
				<p>
				<label class="field" for="id">ID<?php echo in_array('id', $required) ? '<span> *</span>' : ''; ?></label>
				<input type="text" id="name" name="id" class="textbox textbox-150" value="<?php echo isset($data['id']) ? $data['id'] : ''; ?>">
				<?php if(key_exists('id', $maxLengths)) echo "<span class='max-len'>(iki {$maxLengths['id']} simb.)</span>"; ?>
			</p>
			<p>
				<label class="field" for="modelis">Modelis<?php echo in_array('modelis', $required) ? '<span> *</span>' : ''; ?></label>
				<select id="modelis" name="modelis">
					<option value="-1">---------------</option>
					<?php
						// išrenkame visas kategorijas sugeneruoti pasirinkimų lauką
						$brands = $brandsObj->getBrandList();
						foreach($brands as $key => $val) {
							echo "<optgroup label='{$val['pavadinimas']}'>";

							$models = $modelsObj->getModelListByBrand($val['id']);
							foreach($models as $key2 => $val2) {
								$selected = "";
								if(isset($data['modelis']) && $data['modelis'] == $val2['id']) {
									$selected = " selected='selected'";
								}
								echo "<option{$selected} value='{$val2['id']}'>{$val2['pavadinimas']}</option>";
							}
						}
					?>
				</select>
			</p>
			<p>
				<label class="field" for="fk_IRANKIU_GRUPEid">Irankių grupė<?php echo in_array('fk_IRANKIU_GRUPEid', $required) ? '<span> *</span>' : ''; ?></label>
				<select id="fk_IRANKIU_GRUPEid" name="fk_IRANKIU_GRUPEid">
					<option value="-1">Pasirinkite grupę</option>
					<?php
						// išrenkame visas markes
						$brands = $brandsObj->getGroupList();
						foreach($brands as $key => $val) {
							$selected = "";
							if(isset($data['fk_IRANKIU_GRUPEid']) && $data['fk_IRANKIU_GRUPEid'] == $val['id']) {
								$selected = "selected='selected'";
							}
							echo "<option{$selected} value='{$val['id']}'>{$val['pavadinimas']}</option>";
						}
					?>
				</select>
			</p>
			<p>
				<label class="field" for="busena">Būsena<?php echo in_array('busena', $required) ? '<span> *</span>' : ''; ?></label>
				<input type="text" id="busena" name="busena" class="textbox textbox-70" value="<?php echo isset($data['busena']) ? $data['busena'] : ''; ?>">
				<?php if(key_exists('busena', $maxLengths)) echo "<span class='max-len'>(iki {$maxLengths['busena']} simb.)</span>"; ?>
			</p>
			<p>
				<label class="field" for="pagaminimo_data">Pagaminimo data<?php echo in_array('pagaminimo_data', $required) ? '<span> *</span>' : ''; ?></label>
				<input type="text" id="pagaminimo_data" name="pagaminimo_data" class="textbox textbox-70 date" value="<?php echo isset($data['pagaminimo_data']) ? $data['pagaminimo_data'] : ''; ?>"></p>

			<p>
				<label class="field" for="greicio_tipai">Greičio tipai<?php echo in_array('greicio_tipai', $required) ? '<span> *</span>' : ''; ?></label>
				<input type="text" id="greicio_tipai" name="greicio_tipai" class="textbox textbox-70" value="<?php echo isset($data['greicio_tipai']) ? $data['greicio_tipai'] : ''; ?>">
			</p>
			<p>
				<label class="field" for="kuro_tipas">Kuro tipas<?php echo in_array('kuro_tipas', $required) ? '<span> *</span>' : ''; ?></label>
				<input type="text" id="kuro_tipas" name="kuro_tipas" class="textbox textbox-70" value="<?php echo isset($data['kuro_tipas']) ? $data['kuro_tipas'] : ''; ?>">
			</p>
			<p>
				<label class="field" for="irankio_tipas">Irankio tipas<?php echo in_array('irankio_tipas', $required) ? '<span> *</span>' : ''; ?></label>
				<input type="text" id="irankio_tipas" name="irankio_tipas" class="textbox textbox-70" value="<?php echo isset($data['irankio_tipas']) ? $data['irankio_tipas'] : ''; ?>">
			</p>
			<p>
				<label class="field" for="galingumas">Galingumas<?php echo in_array('galingumas', $required) ? '<span> *</span>' : ''; ?></label>
				<input type="text" id="galingumas" name="galingumas" class="textbox textbox-70" value="<?php echo isset($data['galingumas']) ? $data['galingumas'] : ''; ?>">
			</p>
			<p>
				<label class="field" for="dydis">Dydis<?php echo in_array('dydis', $required) ? '<span> *</span>' : ''; ?></label>
				<input type="text" id="dydis" name="dydis" class="textbox textbox-70" value="<?php echo isset($data['dydis']) ? $data['dydis'] : ''; ?>">
			</p>
			<p>
				<label class="field" for="svoris">Svoris<?php echo in_array('svoris', $required) ? '<span> *</span>' : ''; ?></label>
				<input type="text" id="svoris" name="svoris" class="textbox textbox-70" value="<?php echo isset($data['svoris']) ? $data['svoris'] : ''; ?>">
			</p>

		</fieldset>
		<p class="required-note">* pažymėtus laukus užpildyti privaloma</p>
		<p>
			<input type="submit" class="submit button" name="submit" value="Išsaugoti">
		</p>
		<?php if(isset($data['id'])) { ?>
			<input type="hidden" name="id" value="<?php echo $data['id']; ?>" />
		<?php } ?>
	</form>
</div>