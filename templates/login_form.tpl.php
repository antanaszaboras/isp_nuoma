<ul id="pagePath">
	
</ul>
<div class="float-clear"></div>
<div id="formContainer">
	<?php if($formErrors != null) { ?>
		<div class="errorBox">
			
			<?php 
				echo $formErrors;
			?>
		</div>
	<?php } ?>
	<form action="" method="post">
		<fieldset>
			<legend>PRISIJUNKITE</legend>
			<p>
				<label class="field" for="el_pastas">Elektroninis paštas<?php echo in_array('el_pastas', $required) ? '<span> *</span>' : ''; ?></label>
				<input type="email" id="el_pastas" name="el_pastas" class="textbox textbox-150" value="<?php echo isset($data['el_pastas']) ? $data['el_pastas'] : ''; ?>" />
			</p>
			<p>
				<label class="field" for="slaptazodis">Slaptažodis<?php echo in_array('slaptazodis', $required) ? '<span> *</span>' : ''; ?></label>
				<input type="password" id="slaptazodis" name="slaptazodis" class="textbox textbox-150" value="<?php echo isset($data['slaptazodis']) ? $data['slaptazodis'] : ''; ?>" />
			</p>
		</fieldset>
		<p class="required-note">* pažymėtus laukus užpildyti privaloma</p>
		<p>
			<input type="submit" class="submit button" name="submit" value="Prisijungti">
		</p>
	</form>
</div>