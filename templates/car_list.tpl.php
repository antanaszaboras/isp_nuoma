<ul id="pagePath">
	<li><a href="index.php">Pradžia</a></li>
	<li>Įrankiai</li>
</ul>
<div id="actions">
	<a href='index.php?module=<?php echo $module; ?>&action=create'>Naujas įrankis</a>
</div>
<div class="float-clear"></div>

<?php if(isset($_GET['remove_error'])) { ?>
	<div class="errorBox">
		Įrankis nebuvo pašalintas, nes yra įtrauktas į sutartį (-is).
	</div>
<?php } ?>

<table class="listTable">
	<tr>
		<th>ID</th>
		<th>Firma</th>
		<th>Modelis</th>
		<th>Būsena</th>
		<th>Pagaminimo data</th>
		<th>Irankio tipas</th>
		<th></th>
	</tr>
	<?php
		// suformuojame lentelę
		foreach($data as $key => $val) {
			echo
				"<tr>"
					. "<td>{$val['id']}</td>"
					. "<td>{$val['firma']}</td>"
					. "<td>{$val['modelis']}</td>"
					. "<td>{$val['busena']}</td>"
					. "<td>{$val['pagaminimo_data']}</td>"
					. "<td>{$val['irankio_tipas']}</td>"
					. "<td>"
						. "<a href='#' onclick='showConfirmDialog(\"{$module}\", \"{$val['id']}\"); return false;' title=''>šalinti</a>&nbsp;"
						. "<a href='index.php?module={$module}&action=edit&id={$val['id']}' title=''>redaguoti</a>"
					. "</td>"
				. "</tr>";
		}
	?>
</table>

<?php
	// įtraukiame puslapių šabloną
	include 'templates/paging.tpl.php';
?>