<ul id="pagePath">
	<li><a href="index.php">Pradžia</a></li>
	<li><a href="index.php?module=<?php echo $module; ?>&action=list">Sutartys</a></li>
	<li><?php if(!empty($id)) echo "Sutarties redagavimas"; else echo "Nauja sutartis"; ?></li>
</ul>
<div class="float-clear"></div>
<div id="formContainer">
	<?php if($formErrors != null) { ?>
		<div class="errorBox">
			Neįvesti arba neteisingai įvesti šie laukai:
			<?php 
				echo $formErrors;
			?>
		</div>
	<?php } ?>
	<form action="" method="post">
		<fieldset>
			<legend>Sutarties informacija</legend>
			<p>
				<?php if(!isset($data['editing'])) { ?>
					<label class="field" for="nr">Numeris<?php echo in_array('nr', $required) ? '<span> *</span>' : ''; ?></label>
					<input type="text" id="nr" name="nr" class="textbox textbox-70" value="<?php echo isset($data['nr']) ? $data['nr'] : ''; ?>">
				<?php } else { ?>
						<label class="field" for="nr">Numeris</label>
						<span class="input-value"><?php echo $data['nr']; ?></span>
						<input type="hidden" name="editing" value="1" />
						<input type="hidden" name="nr" value="<?php echo $data['nr']; ?>" />
				<?php } ?>
			</p>
			<p>
				<label class="field" for="sutarties_data">Data<?php echo in_array('sutarties_data', $required) ? '<span> *</span>' : ''; ?></label>
				<input type="text" id="sutarties_data" name="sutarties_data" class="textbox date textbox-70" value="<?php echo isset($data['sutarties_data']) ? $data['sutarties_data'] : ''; ?>">
			</p>
			<p>
				<label class="field" for="fk_KLIENTASasmens_kodas">Klientas<?php echo in_array('fk_KLIENTASasmens_kodas', $required) ? '<span> *</span>' : ''; ?></label>
				<select id="fk_KLIENTASasmens_kodas" name="fk_KLIENTASasmens_kodas">
					<option value="">---------------</option>
					<?php
						// išrenkame klientus
						$customers = $customersObj->getCustomersList();
						foreach($customers as $key => $val) {
							$selected = "";
							if(isset($data['fk_KLIENTASasmens_kodas']) && $data['fk_KLIENTASasmens_kodas'] == $val['asmens_kodas']) {
								$selected = " selected='selected'";
							}
							echo "<option{$selected} value='{$val['asmens_kodas']}'>{$val['vardas']} {$val['pavarde']}</option>";
						}
					?>
				</select>
			</p>
			<p>
				<label class="field" for="fk_DARBUOTOJAStabelio_nr">Darbuotojas<?php echo in_array('fk_DARBUOTOJAStabelio_nr', $required) ? '<span> *</span>' : ''; ?></label>
				<select id="fk_DARBUOTOJAStabelio_nr" name="fk_DARBUOTOJAStabelio_nr">
					<option value="">---------------</option>
					<?php
						// išrenkame vartotojus
						$employees = $employeesObj->getEmplyeesList();
						foreach($employees as $key => $val) {
							$selected = "";
							if(isset($data['fk_DARBUOTOJAStabelio_nr']) && $data['fk_DARBUOTOJAStabelio_nr'] == $val['tabelio_nr']) {
								$selected = " selected='selected'";
							}
							echo "<option{$selected} value='{$val['tabelio_nr']}'>{$val['vardas']} {$val['pavarde']}</option>";
						}
					?>
				</select>
			</p>
			<p>
				<label class="field" for="fk_NUOMOS_TARIFASgalioja_nuo">Tarifo data<?php echo in_array('fk_NUOMOS_TARIFASgalioja_nuo', $required) ? '<span> *</span>' : ''; ?></label>
				<input type="text" id="fk_NUOMOS_TARIFASgalioja_nuo" name="fk_NUOMOS_TARIFASgalioja_nuo" class="textbox date textbox-150" value="<?php echo isset($data['fk_NUOMOS_TARIFASgalioja_nuo']) ? $data['fk_NUOMOS_TARIFASgalioja_nuo'] : ''; ?>">
			</p>
			<p>
				<label class="field" for="nuomos_data">Nuomos data ir laikas<?php echo in_array('nuomos_data', $required) ? '<span> *</span>' : ''; ?></label>
				<input type="text" id="nuomos_data" name="nuomos_data" class="textbox datetime textbox-150" value="<?php echo isset($data['nuomos_data']) ? $data['nuomos_data'] : ''; ?>">
			</p>
			<p>
				<label class="field" for="planuojama_grazinimo_data">Planuojama grąžinti<?php echo in_array('planuojama_grazinimo_data', $required) ? '<span> *</span>' : ''; ?></label>
				<input type="text" id="planuojama_grazinimo_data" name="planuojama_grazinimo_data" class="textbox datetime textbox-150" value="<?php echo isset($data['planuojama_grazinimo_data']) ? $data['planuojama_grazinimo_data'] : ''; ?>">
			</p>
			<p>
				<label class="field" for="faktine_grazinimo_data">Grąžinta<?php echo in_array('faktine_grazinimo_data', $required) ? '<span> *</span>' : ''; ?></label>
				<input type="text" id="faktine_grazinimo_data" name="faktine_grazinimo_data" class="textbox datetime textbox-150" value="<?php echo isset($data['faktine_grazinimo_data']) ? $data['faktine_grazinimo_data'] : ''; ?>">
			</p>
			<p>
				<label class="field" for="busena">Būsena<?php echo in_array('busena', $required) ? '<span> *</span>' : ''; ?></label>
				<input type="text" id="busena" name="busena" class="textbox textbox-70" value="<?php echo isset($data['busena']) ? $data['busena'] : ''; ?>">
			</p>
			<p>
				<label class="field" for="kaina">Nuomos kaina<?php echo in_array('kaina', $required) ? '<span> *</span>' : ''; ?></label>
				<input type="text" id="kaina" name="kaina" class="textbox textbox-70" value="<?php echo isset($data['kaina']) ? $data['kaina'] : ''; ?>"> <span class="units">&euro;</span>
			</p>
			<p>
				<label class="field" for="bendra_kaina">Bendra kaina su paslaugomis</label><span class="units"><?php echo isset($data['bendra_kaina']) ? $data['bendra_kaina'] . ' &euro;' : ''; ?></span>
				<input type="hidden" name="bendra_kaina" value="<?php echo isset($data['bendra_kaina']) ? $data['bendra_kaina'] : ''; ?>" />
			</p>
		</fieldset>

		<fieldset>
			<legend>Įrankio informacija</legend>
			<p>
				<label class="field" for="fk_IRANKISid">Įrankis<?php echo in_array('fk_IRANKISid', $required) ? '<span> *</span>' : ''; ?></label>
				<select id="fk_IRANKISid" name="fk_IRANKISid">
					<option value="">---------------</option>
					<?php
						// išrenkame automobilius
						$cars = $carsObj->getCarList();
						foreach($cars as $key => $val) {
							$selected = "";
							if(isset($data['fk_IRANKISid']) && $data['fk_IRANKISid'] == $val['id']) {
								$selected = " selected='selected'";
							}
							echo "<option{$selected} value='{$val['id']}'>{$val['id']} - {$val['firma']} {$val['modelis']}</option>";
						}
					?>
				</select>
			</p>
			<p>
				<label class="field" for="fk_SANDELISid">Paėmimo vieta<?php echo in_array('fk_SANDELISid', $required) ? '<span> *</span>' : ''; ?></label>
				<select id="fk_SANDELISid" name="fk_SANDELISid">
					<option value="">---------------</option>
					<?php
						// išrenkame aikšteles
						$parkingLots = $contractsObj->getParkingLots();
						foreach($parkingLots as $key => $val) {
							$selected = "";
							if(isset($data['fk_SANDELISid']) && $data['fk_SANDELISid'] == $val['id']) {
								$selected = " selected='selected'";
							}
							echo "<option{$selected} value='{$val['id']}'>{$val['pavadinimas']}</option>";
						}
					?>
				</select>
			</p>
			<p>
				<label class="field" for="pradine_bukle">Būklė paimant<?php echo in_array('pradine_bukle', $required) ? '<span> *</span>' : ''; ?></label>
				<input type="text" id="pradine_bukle" name="pradine_bukle" class="textbox textbox-30" value="<?php echo isset($data['pradine_bukle']) ? $data['pradine_bukle'] : ''; ?>">
			</p>
		
			<p>
				<label class="field" for="fk_SANDELISid1">Grąžinimo vieta<?php echo in_array('fk_SANDELISid1', $required) ? '<span> *</span>' : ''; ?></label>
				<select id="fk_SANDELISid1" name="fk_SANDELISid1">
					<option value="">---------------</option>
					<?php
						// išrenkame aikšteles
						$parkingLots = $contractsObj->getParkingLots();
						foreach($parkingLots as $key => $val) {
							$selected = "";
							if(isset($data['fk_SANDELISid1']) && $data['fk_SANDELISid1'] == $val['id']) {
								$selected = " selected='selected'";
							}
							echo "<option{$selected} value='{$val['id']}'>{$val['pavadinimas']}</option>";
						}
					?>
				</select>
			</p>
			<p>
				<label class="field" for="galutine_bukle">Būklė grąžinus<?php echo in_array('galutine_bukle', $required) ? '<span> *</span>' : ''; ?></label>
				<input type="text" id="galutine_bukle" name="galutine_bukle" class="textbox textbox-30" value="<?php echo isset($data['galutine_bukle']) ? $data['galutine_bukle'] : ''; ?>">
			</p>
		</fieldset>

		<fieldset>
			<legend>Papildomos paslaugos</legend>
			<div class="childRowContainer">
				<div class="labelLeft wide<?php if(empty($data['uzsakytos_paslaugos']) || sizeof($data['uzsakytos_paslaugos']) == 0) echo ' hidden'; ?>">Paslauga</div>
				<div class="labelRight<?php if(empty($data['uzsakytos_paslaugos']) || sizeof($data['uzsakytos_paslaugos']) == 0) echo ' hidden'; ?>">Kiekis</div>
				<div class="float-clear"></div>
				<?php
					if(empty($data['uzsakytos_paslaugos']) || sizeof($data['uzsakytos_paslaugos']) == 0) {
				?>
					<div class="childRow hidden">
						<select class="elementSelector" name="paslaugos[]" disabled="disabled">
							<?php
								$tmp = $servicesObj->getServicesList();
								foreach($tmp as $key1 => $val1) {
									echo "<optgroup label='{$val1['pavadinimas']}'>";
									$tmp = $servicesObj->getServicePrices($val1['id']);
									foreach($tmp as $key2 => $val2) {
										$selected = "";
										if(isset($data['modelis']) && $data['modelis'] == $val2['id']) {
											$selected = " selected='selected'";
										}
										echo "<option{$selected} value='{$val2['fk_paslauga']}:{$val2['kaina']}:{$val2['galioja_nuo']}'>{$val1['pavadinimas']} {$val2['kaina']} EUR (nuo {$val2['galioja_nuo']})</option>";
									}
								}
							?>
						</select>
						<input type="text" name="kiekiai[]" class="textbox textbox-30" value="" disabled="disabled" />
						<a href="#" title="" class="removeChild">šalinti</a>
					</div>
					<div class="float-clear"></div>

				<?php
					} else {
						foreach($data['uzsakytos_paslaugos'] as $key => $val) {
				?>
						<div class="childRow">
							<select class="elementSelector" name="paslaugos[]">
								<?php
									$tmp = $servicesObj->getServicesList();
									foreach($tmp as $key1 => $val1) {
										echo "<optgroup label='{$val1['pavadinimas']}'>";
										$tmp = $servicesObj->getServicePrices($val1['id']);
										foreach($tmp as $key2 => $val2) {
											$selected = "";
											if($val['fk_kaina_galioja_nuo'] == $val2['galioja_nuo'] && $val['fk_paslauga'] == $val2['fk_paslauga']) {
												$selected = " selected='selected'";
											}
											echo "<option{$selected} value='{$val2['fk_paslauga']}:{$val2['kaina']}:{$val2['galioja_nuo']}'>{$val1['pavadinimas']} {$val2['kaina']} EUR (nuo {$val2['galioja_nuo']})</option>";
										}
									}
								?>
							</select>
							<input type="text" name="kiekiai[]" class="textbox textbox-30" value="<?php echo isset($val['kiekis']) ? $val['kiekis'] : ''; ?>" />
							<a href="#" title="" class="removeChild">šalinti</a>
						</div>
						<div class="float-clear"></div>
				<?php 
						}
					}
				?>
			</div>
			<p id="newItemButtonContainer">
				<a href="#" title="" class="addChild">Pridėti</a>
			</p>
		</fieldset>
		
		<p class="required-note">* pažymėtus laukus užpildyti privaloma</p>
		<p>
			<input type="submit" class="submit button" name="submit" value="Išsaugoti">
		</p>

		<input type="hidden" name="id" value="<?php echo isset($data['id']) ? $data['id'] : ''; ?>" />
	</form>
</div>