<?php
/**
 * Irankių redagavimo klasė
 *
 * @author ISK
 */

 
  ini_set("xdebug.var_display_max_children", -1);
ini_set("xdebug.var_display_max_data", -1);
ini_set("xdebug.var_display_max_depth", -1);
class cars {

	private $firmos_lentele = ''; 
	private $modeliai_lentele = '';
	private $irankiai_lentele = '';
	private $irankio_busenos_lentele = '';
	private $sutartys_lentele = '';
	
	public function __construct() {
		$this->firmos_lentele = config::DB_PREFIX . 'FIRMA';
		$this->modeliai_lentele = config::DB_PREFIX . 'MODELIS';
		$this->irankiai_lentele = config::DB_PREFIX . 'IRANKIS';
		$this->sutartys_lentele = config::DB_PREFIX . 'SUTARTIS';
	}
	
	/**
	 * Įrankio išrinkimas
	 * @param type $id
	 * @return type
	 */
	public function getCar($id) {
		$query = "  SELECT `{$this->irankiai_lentele}`.`id`,
						   `{$this->irankiai_lentele}`.`pagaminimo_data`,
						   `{$this->irankiai_lentele}`.`greicio_tipai`,
						   `{$this->irankiai_lentele}`.`kuro_tipas`,
						   `{$this->irankiai_lentele}`.`irankio_tipas`,
						   `{$this->irankiai_lentele}`.`galingumas`,
						   `{$this->irankiai_lentele}`.`dydis`,
						   `{$this->irankiai_lentele}`.`busena`,
						   `{$this->irankiai_lentele}`.`svoris`,
						   `{$this->irankiai_lentele}`.`fk_MODELISid` AS `modelis`,
						   `{$this->irankiai_lentele}`.`fk_IRANKIU_GRUPEid`
					FROM `{$this->irankiai_lentele}`
					WHERE `{$this->irankiai_lentele}`.`id`='{$id}'";

		$data = mysql::select($query);
		
		return $data[0];
	}
	
	/**
	 * Įrankio atnaujinimas
	 * @param type $data
	 */
	public function updateCar($data) {
		$query = "  UPDATE `{$this->irankiai_lentele}`
					SET    `pagaminimo_data`='{$data['pagaminimo_data']}',
						   `greicio_tipai`='{$data['greicio_tipai']}',
						   `kuro_tipas`='{$data['kuro_tipas']}',
						   `irankio_tipas`='{$data['irankio_tipas']}',
						   `galingumas`='{$data['galingumas']}',
						   `dydis`='{$data['dydis']}',
						   `busena`='{$data['busena']}',
						   `svoris`='{$data['svoris']}',
						   `fk_MODELISid`='{$data['modelis']}',
						   `fk_IRANKIU_GRUPEid`='{$data['irankiu_grupe']}'
					WHERE `id`='{$data['id']}'";
		mysql::query($query);
	}

	/**
	 * Įrankio įrašymas
	 * @param type $data
	 */
	public function insertCar($data) {
		$query = "  INSERT INTO `{$this->irankiai_lentele}` 
								(
									`pagaminimo_data`,
									`greicio_tipai`,
									`kuro_tipas`,
									`irankio_tipas`,
									`galingumas`,
									`dydis`,
									`busena`,
									`svoris`,
									`fk_MODELISid`,
									`fk_IRANKIU_GRUPEid`
								) 
								VALUES
								(
									'{$data['pagaminimo_data']}',
									'{$data['greicio_tipai']}',
									'{$data['kuro_tipas']}',
									'{$data['irankio_tipas']}',
									'{$data['galingumas']}',
									'{$data['dydis']}',
									'{$data['busena']}',
									'{$data['svoris']}',
									'{$data['modelis']}',
									'{$data['irankiu_grupe']}'
								)";
								var_dump($data['irankiu_grupe']);
					exit;
		mysql::query($query);
	}
	
	/**
	 * Įrankių sąrašo išrinkimas
	 * @param type $limit
	 * @param type $offset
	 * @return type
	 */
	public function getCarList($limit = null, $offset = null) {
		$limitOffsetString = "";
		if(isset($limit)) {
			$limitOffsetString .= " LIMIT {$limit}";
		}
		if(isset($offset)) {
			$limitOffsetString .= " OFFSET {$offset}";
		}
		
		$query = "  SELECT `{$this->irankiai_lentele}`.`id`,
						   `{$this->irankiai_lentele}`.`pagaminimo_data`,
						   `{$this->irankiai_lentele}`.`busena`,
						   `{$this->irankiai_lentele}`.`dydis`,
						   `{$this->irankiai_lentele}`.`svoris`,
						   `{$this->irankiai_lentele}`.`kuro_tipas`,
						   `{$this->irankiai_lentele}`.`irankio_tipas`,
						   `{$this->irankiai_lentele}`.`greicio_tipai`,
						   `{$this->modeliai_lentele}`.`pavadinimas` AS `modelis`,
						   `{$this->firmos_lentele}`.`pavadinimas` AS `firma`,
						   `{$this->irankiai_lentele}`.`fk_IRANKIU_GRUPEid` AS `irankiu_grupe`
					FROM `{$this->irankiai_lentele}`
						LEFT JOIN `{$this->modeliai_lentele}`
							ON `{$this->irankiai_lentele}`.`fk_MODELISid`=`{$this->modeliai_lentele}`.`id`
						LEFT JOIN `{$this->firmos_lentele}`
							ON `{$this->modeliai_lentele}`.`fk_FIRMAid`=`{$this->firmos_lentele}`.`id`" . $limitOffsetString;

		$data = mysql::select($query);
		
		return $data;
	}

	/**
	 * Įrankių kiekio radimas
	 * @return type
	 */
	public function getCarListCount() {
		$query = "  SELECT COUNT(`{$this->irankiai_lentele}`.`id`) AS `kiekis`
					FROM `{$this->irankiai_lentele}`
						LEFT JOIN `{$this->modeliai_lentele}`
							ON `{$this->irankiai_lentele}`.`fk_MODELISid`=`{$this->modeliai_lentele}`.`id`
						LEFT JOIN `{$this->firmos_lentele}` 
							ON `{$this->modeliai_lentele}`.`fk_FIRMAid`=`{$this->firmos_lentele}`.`id`";
		$data = mysql::select($query);
		
		return $data[0]['kiekis'];
	}
	
	/**
	 * Įrankio šalinimas
	 * @param type $id
	 */
	public function deleteCar($id) {
		$query = "  DELETE FROM `{$this->irankiai_lentele}`
					WHERE `id`='{$id}'";
		mysql::query($query);
	}
	
	/**
	 * Sutačių, į kurias įtrauktas įrankis, kiekio radimas
	 * @param type $id
	 * @return type
	 */
	public function getContractCountOfCar($id) {
		$query = "  SELECT COUNT(`{$this->sutartys_lentele}`.`nr`) AS `kiekis`
					FROM `{$this->irankiai_lentele}`
						INNER JOIN `{$this->sutartys_lentele}`
							ON `{$this->irankiai_lentele}`.`id`=`{$this->sutartys_lentele}`.`fk_IRANKISid`
					WHERE `{$this->irankiai_lentele}`.`id`='{$id}'";
		$data = mysql::select($query);
		
		return $data[0]['kiekis'];
	}
	


	
}