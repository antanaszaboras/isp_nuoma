<?php
/**
 * Naudotojų klasė
 *
 * @author Antnas Zaboras
 */

class users {
	
	private $naudotojai_lentele = '';
	private $busenos_lentele = '';
	private $roles_lentele = '';
	//private $sutartys_lentele = '';
	
	public function __construct() {
		$this->naudotojai_lentele = config::DB_PREFIX . 'NAUDOTOJAS';
		$this->busenos_lentele = config::DB_PREFIX . 'NAUDOTOJO_BUSENA';
		$this->roles_lentele = config::DB_PREFIX . 'NAUDOTOJO_ROLE';
		//$this->sutartys_lentele = config::DB_PREFIX . 'SUTARTIS';
	}
	
	/**
	 * Naudotojoišrinkimas
	 * @param type $id
	 * @return type
	 */
	public function getUser($id) {
		$query = "  SELECT 
					`{$this->naudotojai_lentele}`.id,
					`{$this->naudotojai_lentele}`.el_pastas,
					`{$this->naudotojai_lentele}`.fk_NAUDOTOJO_BUSENAid AS busena,
					`{$this->naudotojai_lentele}`.fk_NAUDOTOJO_ROLEid AS role,
					`{$this->naudotojai_lentele}`.sukurimo_data,
					`{$this->naudotojai_lentele}`.redagavimo_data,
					`{$this->naudotojai_lentele}`.prisijungimo_data,
					`{$this->busenos_lentele}`.pavadinimas AS busenos_pavadinimas,
					`{$this->busenos_lentele}`.komentaras AS busenos_komentaras,
					`{$this->busenos_lentele}`.galima_prisijungti,
					`{$this->roles_lentele}`.pavadinimas AS roles_pavadinimas,
					`{$this->roles_lentele}`.komentaras AS roles_komentaras,
					`{$this->roles_lentele}`.darbuotojas,
					`{$this->roles_lentele}`.administratorius
					FROM `{$this->naudotojai_lentele}`
					JOIN `{$this->busenos_lentele}` ON `{$this->busenos_lentele}`.id = `{$this->naudotojai_lentele}`.fk_NAUDOTOJO_BUSENAid
					JOIN `{$this->roles_lentele}` ON `{$this->roles_lentele}`.id = `{$this->naudotojai_lentele}`.fk_NAUDOTOJO_ROLEid
					WHERE `{$this->naudotojai_lentele}`.id ='{$id}'";
		$data = mysql::select($query);
		if($data[0]['darbuotojas']){
			$query = " SELECT darbuotojas.vardas, darbuotojas.pavarde 
						FROM darbuotojas
						WHERE fk_NAUDOTOJASid = '" . $data[0]['id'] . "'";
		}else{
			$query = " SELECT klientas.vardas, klientas.pavarde 
						FROM klientas
						WHERE fk_NAUDOTOJASid = '" . $data[0]['id'] . "'";
		}
		$data2 = mysql::select($query);
		$data[0]['vardas'] = $data2[0]['vardas'];
		$data[0]['pavarde'] = $data2[0]['pavarde'];
		return $data[0];
	}
	
	public function checkUsernamePassword($email, $password){
		
		$encryptedPassword = md5($password);
		$query = " SELECT `{$this->naudotojai_lentele}`.id,
				   `{$this->busenos_lentele}`.galima_prisijungti
				   FROM `{$this->naudotojai_lentele}`
				   LEFT JOIN `{$this->busenos_lentele}` ON `{$this->busenos_lentele}`.id = `{$this->naudotojai_lentele}`.fk_NAUDOTOJO_BUSENAid
				   WHERE `el_pastas` = '{$email}'
				   AND `slaptazodis` = '{$encryptedPassword}'
		";
		$result = mysql::select($query);
		if(!empty($result)){
			if($result[0]['galima_prisijungti'])
				return $result[0]['id'];
			return 0;
		}
		return -1;
	}
	
	/**
	 * Naudotojų sąrašo išrinkimas
	 * @param type $limit
	 * @param type $offset
	 * @return type
	 */
	 /*
	public function getCustomersList($limit = null, $offset = null) {
		$limitOffsetString = "";
		if(isset($limit)) {
			$limitOffsetString .= " LIMIT {$limit}";
		}
		if(isset($offset)) {
			$limitOffsetString .= " OFFSET {$offset}";
		}
		
		$query = "  SELECT *
					FROM `{$this->klientai_lentele}`" . $limitOffsetString;
		$data = mysql::select($query);
		
		return $data;
	}
	*/
	
	/**
	 * Klientų kiekio radimas
	 * @return type
	 */
	 /*
	public function getCustomersListCount() {
		$query = "  SELECT COUNT(`asmens_kodas`) as `kiekis`
					FROM `{$this->klientai_lentele}`";
		$data = mysql::select($query);
		
		return $data[0]['kiekis'];
	}
	*/
	
	/**
	 * Kliento šalinimas
	 * @param type $id
	 */
	 /*
	public function deleteCustomer($id) {
		$query = "  DELETE FROM `{$this->klientai_lentele}`
					WHERE `asmens_kodas`='{$id}'";
		mysql::query($query);
	}
	*/
	/**
	 * Kliento atnaujinimas
	 * @param type $data
	 */
	 /*
	public function updateCustomer($data) {
		$query = "  UPDATE `{$this->klientai_lentele}`
					SET    `vardas`='{$data['vardas']}',
						   `pavarde`='{$data['pavarde']}',
						   `gimimo_data`='{$data['gimimo_data']}',
						   `telefonas`='{$data['telefonas']}',
						   `el_pastas`='{$data['el_pastas']}'
					WHERE `asmens_kodas`='{$data['asmens_kodas']}'";
		mysql::query($query);
	}
	*/
	
	/**
	 * Kliento įrašymas
	 * @param type $data
	 */
	 /*
	public function insertCustomer($data) {
		$query = "  INSERT INTO `{$this->klientai_lentele}`
								(
									`asmens_kodas`,
									`vardas`,
									`pavarde`,
									`gimimo_data`,
									`telefonas`,
									`el_pastas`
								) 
								VALUES
								(
									'{$data['asmens_kodas']}',
									'{$data['vardas']}',
									'{$data['pavarde']}',
									'{$data['gimimo_data']}',
									'{$data['telefonas']}',
									'{$data['el_pastas']}'
								)";
		mysql::query($query);
	}*/
	
	/**
	 * Sutarčių, į kurias įtrauktas klientas, kiekio radimas
	 * @param type $id
	 * @return type
	 */
	 /*
	public function getContractCountOfCustomer($id) {
		$query = "  SELECT COUNT(`{$this->sutartys_lentele}`.`nr`) AS `kiekis`
					FROM `{$this->klientai_lentele}`
						INNER JOIN `{$this->sutartys_lentele}`
							ON `{$this->klientai_lentele}`.`asmens_kodas`=`{$this->sutartys_lentele}`.`fk_KLIENTASasmens_kodas`
					WHERE `{$this->klientai_lentele}`.`asmens_kodas`='{$id}'";
		$data = mysql::select($query);
		
		return $data[0]['kiekis'];
	}
	*/
}