<?php
/**
 * Darbuotojų redagavimo klasė
 *
 * @author ISK
 */

class employees {
	
	private $darbuotojai_lentele = '';
	private $sutartys_lentele = '';
	
	public function __construct() {
		$this->darbuotojai_lentele = 'DARBUOTOJAS';
		$this->sutartys_lentele = 'SUTARTIS';
	}
	
	/**
	 * Darbuotojo išrinkimas
	 * @param type $id
	 * @return type
	 */
	public function getEmployee($id) {
		$query = "  SELECT *
					FROM `{$this->darbuotojai_lentele}`
					WHERE `tabelio_nr`='{$id}'";
		$data = mysql::select($query);
		
		return $data[0];
	}
	
	/**
	 * Darbuotojų sąrašo išrinkimas
	 * @param type $limit
	 * @param type $offset
	 * @return type
	 */
	public function getEmplyeesList($limit = null, $offset = null) {
		$limitOffsetString = "";
		if(isset($limit)) {
			$limitOffsetString .= " LIMIT {$limit}";
		}
		if(isset($offset)) {
			$limitOffsetString .= " OFFSET {$offset}";
		}
		
		$query = "  SELECT *
					FROM `{$this->darbuotojai_lentele}`" . $limitOffsetString;
		$data = mysql::select($query);
		
		return $data;
	}
	
	/**
	 * Darbuotojų kiekio radimas
	 * @return type
	 */
	public function getEmplyeesListCount() {
		$query = "  SELECT COUNT(`tabelio_nr`) as `kiekis`
					FROM `{$this->darbuotojai_lentele}`";
		$data = mysql::select($query);
		
		return $data[0]['kiekis'];
	}
	
	/**
	 * Darbuotojo šalinimas
	 * @param type $id
	 */
	public function deleteEmployee($id) {
		$query = "  DELETE FROM `{$this->darbuotojai_lentele}`
					WHERE `tabelio_nr`='{$id}'";
		mysql::query($query);
	}
	
	/**
	 * Darbuotojo atnaujinimas
	 * @param type $data
	 */
	public function updateEmployee($data) {
		$query = "  UPDATE `{$this->darbuotojai_lentele}`
					SET    `vardas`='{$data['vardas']}',
						   `pavarde`='{$data['pavarde']}',
						   `fk_NUOMOS_BIURASid`='{$data['fk_NUOMOS_BIURASid']}'
					WHERE `tabelio_nr`='{$data['tabelio_nr']}'";
		mysql::query($query);
	}
	
	/**
	 * Darbuotojo įrašymas
	 * @param type $data
	 */
	public function insertEmployee($data) {
		$query = "  INSERT INTO `{$this->darbuotojai_lentele}`
								(
									`tabelio_nr`,
									`vardas`,
									`pavarde`,
									`fk_NUOMOS_BIURASid`
								) 
								VALUES
								(
									'{$data['tabelio_nr']}',
									'{$data['vardas']}',
									'{$data['pavarde']}',
									'{$data['fk_NUOMOS_BIURASid']}'
								)";
		mysql::query($query);
	}
	
	/**
	 * Sutarčių, į kurias įtrauktas darbuotojas, kiekio radimas
	 * @param type $id
	 * @return type
	 */
	public function getContractCountOfEmployee($id) {
		$query = "  SELECT COUNT(`{$this->sutartys_lentele}`.`nr`) AS `kiekis`
					FROM `{$this->darbuotojai_lentele}`
						INNER JOIN `{$this->sutartys_lentele}`
							ON `{$this->darbuotojai_lentele}`.`tabelio_nr`=`{$this->sutartys_lentele}`.`fk_DARBUOTOJAStabelio_nr`
					WHERE `{$this->darbuotojai_lentele}`.`tabelio_nr`='{$id}'";
		$data = mysql::select($query);
		
		return $data[0]['kiekis'];
	}
	
}