<?php
/**
 * Automobilių markių redagavimo klasė
 *
 * @author ISK
 */

class brands {
	
	private $firmos_lentele = '';
	private $modeliai_lentele = '';
	private $grupes_lentele = '';
	
	public function __construct() {
		$this->firmos_lentele = config::DB_PREFIX . 'FIRMA';
		$this->modeliai_lentele = config::DB_PREFIX . 'MODELIS';
		$this->grupes_lentele = config::DB_PREFIX . 'IRANKIU_GRUPE';
	}
	
	/**
	 * Markės išrinkimas
	 * @param type $id
	 * @return type
	 */
	public function getBrand($id) {
		$query = "  SELECT *
					FROM {$this->firmos_lentele}
					WHERE `id`='{$id}'";
		$data = mysql::select($query);
		
		return $data[0];
	}
	
	/**
	 * Markių sąrašo išrinkimas
	 * @param type $limit
	 * @param type $offset
	 * @return type
	 */
	public function getBrandList($limit = null, $offset = null) {
		$limitOffsetString = "";
		if(isset($limit)) {
			$limitOffsetString .= " LIMIT {$limit}";
			
			if(isset($offset)) {
				$limitOffsetString .= " OFFSET {$offset}";
			}	
		}
		
		$query = "  SELECT *
					FROM {$this->firmos_lentele}{$limitOffsetString}";
		$data = mysql::select($query);
		
		return $data;
	}
	
	public function getGroupList($limit = null, $offset = null) {
		$limitOffsetString = "";
		if(isset($limit)) {
			$limitOffsetString .= " LIMIT {$limit}";
			
			if(isset($offset)) {
				$limitOffsetString .= " OFFSET {$offset}";
			}	
		}
		
		$query = "  SELECT *
					FROM {$this->grupes_lentele}{$limitOffsetString}";
		$data = mysql::select($query);
		
		return $data;
	}

	/**
	 * Markių kiekio radimas
	 * @return type
	 */
	public function getBrandListCount() {
		$query = "  SELECT COUNT(`id`) as `kiekis`
					FROM {$this->firmos_lentele}";
		$data = mysql::select($query);
		
		return $data[0]['kiekis'];
	}
	
	/**
	 * Markės įrašymas
	 * @param type $data
	 */
	public function insertBrand($data) {
		$query = "  INSERT INTO {$this->firmos_lentele}
								(
									`pavadinimas`,
									`id`
									
								)
								VALUES
								(
									'{$data['pavadinimas']}',
									'{$data['id']}'
								)";
	
		mysql::query($query);
	}
	
	/**
	 * Markės atnaujinimas
	 * @param type $data
	 */
	public function updateBrand($data) {
		$query = "  UPDATE {$this->firmos_lentele}
					SET    `pavadinimas`='{$data['pavadinimas']}'
					WHERE `id`='{$data['id']}'";
		mysql::query($query);
	}
	
	/**
	 * Markės šalinimas
	 * @param type $id
	 */
	public function deleteBrand($id) {
		$query = "  DELETE FROM {$this->firmos_lentele}
					WHERE `id`='{$id}'";
		mysql::query($query);
	}
	
	/**
	 * Markės modelių kiekio radimas
	 * @param type $id
	 * @return type
	 */
	public function getModelCountOfBrand($id) {
		$query = "  SELECT COUNT({$this->modeliai_lentele}.`id`) AS `kiekis`
					FROM {$this->firmos_lentele}
						INNER JOIN {$this->modeliai_lentele}
							ON {$this->firmos_lentele}.`id`={$this->modeliai_lentele}.`fk_FIRMAid`
					WHERE {$this->firmos_lentele}.`id`='{$id}'";
		$data = mysql::select($query);
		
		return $data[0]['kiekis'];
	}

	
}