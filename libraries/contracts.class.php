<?php
/**
 * Sutarčių redagavimo klasė
 *
 * @author ISK
 */
 ini_set("xdebug.var_display_max_children", -1);
ini_set("xdebug.var_display_max_data", -1);
ini_set("xdebug.var_display_max_depth", -1);

class contracts {

	private $sutartys_lentele = '';
	private $darbuotojai_lentele = '';
	private $klientai_lentele = '';
	private $uzsakytos_paslaugos_lentele = '';
	private $nuomos_tarifas = '';
	private $sandelis_lentele = '';
	private $paslaugu_kainos_lentele = '';
	
	public function __construct() {
		$this->nuomos_tarifas = 'NUOMOS_TARIFAS';
		$this->sutartys_lentele = config::DB_PREFIX . 'SUTARTIS';
		$this->darbuotojai_lentele = config::DB_PREFIX . 'DARBUOTOJAS';
		$this->klientai_lentele = config::DB_PREFIX . 'KLIENTAS';
		$this->uzsakytos_paslaugos_lentele = config::DB_PREFIX . 'UZSAKYTA_PASLAUGA';
		$this->sandelis_lentele = config::DB_PREFIX . 'SANDELIS';
		$this->paslaugu_kainos_lentele = config::DB_PREFIX . 'PASLAUGOS_KAINA';
	}
	
	/**
	 * Sutarčių sąrašo išrinkimas
	 * @param type $limit
	 * @param type $offset
	 * @return type
	 */
	public function getContractList($limit, $offset) {
		$query = "  SELECT `{$this->sutartys_lentele}`.`nr`,
						   `{$this->sutartys_lentele}`.`busena`,
						   `{$this->sutartys_lentele}`.`sutarties_data`,
						   `{$this->darbuotojai_lentele}`.`vardas` AS `darbuotojo_vardas`,
						   `{$this->darbuotojai_lentele}`.`pavarde` AS `darbuotojo_pavarde`,
						   `{$this->klientai_lentele}`.`vardas` AS `kliento_vardas`,
						   `{$this->klientai_lentele}`.`pavarde` AS `kliento_pavarde`
					FROM `{$this->sutartys_lentele}`
						LEFT JOIN `{$this->darbuotojai_lentele}`
							ON `{$this->sutartys_lentele}`.`fk_DARBUOTOJAStabelio_nr`=`{$this->darbuotojai_lentele}`.`tabelio_nr`
						LEFT JOIN `{$this->klientai_lentele}`
							ON `{$this->sutartys_lentele}`.`fk_KLIENTASasmens_kodas`=`{$this->klientai_lentele}`.`asmens_kodas` LIMIT {$limit} OFFSET {$offset}";
		$data = mysql::select($query);
		
		return $data;
	}
	
	/**
	 * Sutarčių kiekio radimas
	 * @return type
	 */
	public function getContractListCount() {
		$query = "  SELECT COUNT(`{$this->sutartys_lentele}`.`nr`) AS `kiekis`
					FROM `{$this->sutartys_lentele}`
						LEFT JOIN `{$this->darbuotojai_lentele}`
							ON `{$this->sutartys_lentele}`.`fk_DARBUOTOJAStabelio_nr`=`{$this->darbuotojai_lentele}`.`tabelio_nr`
						LEFT JOIN `{$this->klientai_lentele}`
							ON `{$this->sutartys_lentele}`.`fk_KLIENTASasmens_kodas`=`{$this->klientai_lentele}`.`asmens_kodas`";
		$data = mysql::select($query);
		
		return $data[0]['kiekis'];
	}
	
	/**
	 * Sutarties išrinkimas
	 * @param type $id
	 * @return type
	 */
	public function getContract($id) {
		$query = "  SELECT `{$this->sutartys_lentele}`.`nr`,
						   `{$this->sutartys_lentele}`.`sutarties_data`,
						   `{$this->sutartys_lentele}`.`nuomos_data`,
						   `{$this->sutartys_lentele}`.`planuojama_grazinimo_data`,
						   `{$this->sutartys_lentele}`.`faktine_grazinimo_data`,
						   `{$this->sutartys_lentele}`.`pradine_bukle`,
						   `{$this->sutartys_lentele}`.`galutine_bukle`,
						   `{$this->sutartys_lentele}`.`kaina`,
						   `{$this->sutartys_lentele}`.`busena`,
						   `{$this->sutartys_lentele}`.`fk_KLIENTASasmens_kodas`,
						   `{$this->sutartys_lentele}`.`fk_DARBUOTOJAStabelio_nr`,
						   `{$this->sutartys_lentele}`.`fk_IRANKISid`,
						   `{$this->sutartys_lentele}`.`fk_SANDELISid`,
						   `{$this->sutartys_lentele}`.`fk_SANDELISid1`,
						   `{$this->sutartys_lentele}`.`fk_NUOMOS_TARIFASgalioja_nuo`,
						   (IFNULL(SUM(`{$this->uzsakytos_paslaugos_lentele}`.`kaina` * `{$this->uzsakytos_paslaugos_lentele}`.`kiekis`), 0) + `{$this->sutartys_lentele}`.`kaina`) AS `bendra_kaina`
					FROM `{$this->sutartys_lentele}`
						LEFT JOIN `{$this->uzsakytos_paslaugos_lentele}`
							ON `{$this->sutartys_lentele}`.`nr`=`{$this->uzsakytos_paslaugos_lentele}`.`fk_SUTARTISnr`
					WHERE `{$this->sutartys_lentele}`.`nr`='{$id}' GROUP BY `{$this->sutartys_lentele}`.`nr`";
		$data = mysql::select($query);

		return $data[0];
	}
	
	/**
	 * Užsakytų papildomų paslaugų sąrašo išrinkimas
	 * @param type $orderId
	 * @return type
	 */
	public function getOrderedServices($orderId) {
		$query = "  SELECT *
					FROM `{$this->uzsakytos_paslaugos_lentele}`
					WHERE `fk_SUTARTISnr`='{$orderId}'";
		$data = mysql::select($query);
		
		return $data;
	}
	
		/**
	 * Užsakytų papildomų paslaugų sąrašo išrinkimas
	 * @param type $tarifasID
	 * @return type
	 */
	public function getTarifas($tarifasID) {
		$query = "  SELECT *
					FROM `{$this->nuomos_tarifas}`
					WHERE `fk_NUOMOS_TARIFASgalioja_nuo`='{$tarifasID}'";
		$data = mysql::select($query);
		
		return $data;
	}
	
	/**
	 * Sutarties atnaujinimas
	 * @param type $data
	 */
	public function updateContract($data) {
		$query = "  UPDATE `{$this->sutartys_lentele}`
					SET    `sutarties_data`='{$data['sutarties_data']}',
						   `nuomos_data`='{$data['nuomos_data']}',
						   `planuojama_grazinimo_data`='{$data['planuojama_grazinimo_data']}',
						   `faktine_grazinimo_data`='{$data['faktine_grazinimo_data']}',
						   `pradine_bukle`='{$data['pradine_bukle']}',
						   `galutine_bukle`='{$data['galutine_bukle']}',
						   `kaina`='{$data['kaina']}',
						   `busena`='{$data['busena']}',
						   `fk_KLIENTASasmens_kodas`='{$data['fk_KLIENTASasmens_kodas']}',
						   `fk_DARBUOTOJAStabelio_nr`='{$data['fk_DARBUOTOJAStabelio_nr']}',
						   `fk_IRANKISid`='{$data['fk_IRANKISid']}',
						   `fk_SANDELISid`='{$data['fk_SANDELISid']}',
						   `fk_SANDELISid1`='{$data['fk_SANDELISid1']}'
						   `fk_NUOMOS_TARIFASgalioja_nuo`='{$data['fk_NUOMOS_TARIFASgalioja_nuo']}'
					WHERE `nr`='{$data['nr']}'";
		mysql::query($query);
	}
	
	/**
	 * Sutarties įrašymas
	 * @param type $data
	 */
	public function insertContract($data) {
		$query = "  INSERT INTO `{$this->sutartys_lentele}`
								(
									`nr`,
									`sutarties_data`,
									`nuomos_data`,
									`planuojama_grazinimo_data`,
									`faktine_grazinimo_data`,
									`pradine_bukle`,
									`galutine_bukle`,
									`kaina`,
									`busena`,
									`fk_KLIENTASasmens_kodas`,
									`fk_DARBUOTOJAStabelio_nr`,
									`fk_IRANKISid`,
									`fk_SANDELISid`,
									`fk_SANDELISid1`,
									`fk_NUOMOS_TARIFASgalioja_nuo`
								)
								VALUES
								(
									'{$data['nr']}',
									'{$data['sutarties_data']}',
									'{$data['nuomos_data']}',
									'{$data['planuojama_grazinimo_data']}',
									'{$data['faktine_grazinimo_data']}',
									'{$data['pradine_bukle']}',
									'{$data['galutine_bukle']}',
									'{$data['kaina']}',
									'{$data['busena']}',
									'{$data['fk_KLIENTASasmens_kodas']}',
									'{$data['fk_DARBUOTOJAStabelio_nr']}',
									'{$data['fk_IRANKISid']}',
									'{$data['fk_SANDELISid']}',
									'{$data['fk_SANDELISid1']}',
									'{$data['fk_NUOMOS_TARIFASgalioja_nuo']}'
								)";
								
		mysql::query($query);
	}
	
	/**
	 * Sutarties šalinimas
	 * @param type $id
	 */
	public function deleteContract($id) {
		$query = "  DELETE FROM `{$this->sutartys_lentele}`
					WHERE `nr`='{$id}'";
		mysql::query($query);
	}
	
	/**
	 * Užsakytų papildomų paslaugų šalinimas
	 * @param type $contractId
	 */
	public function deleteOrderedServices($contractId) {
		$query = "  DELETE FROM `{$this->uzsakytos_paslaugos_lentele}`
					WHERE `fk_SUTARTISnr`='{$contractId}'";
		mysql::query($query);
	}
	
	/**
	 * Užsakytų papildomų paslaugų atnaujinimas
	 * @param type $data
	 */
	public function updateOrderedServices($data) {
		$this->deleteOrderedServices($data['nr']);
		
		if(isset($data['paslaugos']) && sizeof($data['paslaugos']) > 0) {
			foreach($data['paslaugos'] as $key=>$val) {
				$tmp = explode(":", $val);
				$serviceId = $tmp[0];
				$price = $tmp[1];
				$date_from = $tmp[2];
				$query = "  INSERT INTO `{$this->uzsakytos_paslaugos_lentele}`
										(
											`fk_SUTARTISnr`,
											`fk_PASLAUGOS_KAINAgalioja_nuo`,
											`kiekis`,
											`kaina`
										)
										VALUES
										(
											'{$data['nr']}',
											'{$date_from}',
											'{$data['kiekiai'][$key]}',
											'{$price}'
										)";
					mysql::query($query);
			}
		}
	}
	

	
	/**
	 * Aikštelių sąrašo išrinkimas
	 * @return type
	 */
	public function getParkingLots() {
		$query = "  SELECT *
					FROM `{$this->sandelis_lentele}`";
		$data = mysql::select($query);
		
		return $data;
	}
	
	/**
	 * Paslaugos kainų įtraukimo į užsakymus kiekio radimas
	 * @param type $serviceId
	 * @param type $validFrom
	 * @return type
	 */
	public function getPricesCountOfOrderedServices($serviceId, $validFrom) {
		$query = "  SELECT COUNT(`{$this->uzsakytos_paslaugos_lentele}`.`fk_PASLAUGOS_KAINAgalioja_nuo`) AS `kiekis`
					FROM `{$this->paslaugu_kainos_lentele}`
						INNER JOIN `{$this->uzsakytos_paslaugos_lentele}`
							ON `{$this->paslaugu_kainos_lentele}`.`fk_PASLAUGAid`=`{$this->uzsakytos_paslaugos_lentele}`.`fk_PASLAUGOS_KAINAgalioja_nuo` AND `{$this->paslaugu_kainos_lentele}`.`galioja_nuo`=`{$this->uzsakytos_paslaugos_lentele}`.`fk_PASLAUGOS_KAINAgalioja_nuo`
					WHERE `{$this->paslaugu_kainos_lentele}`.`fk_paslauga`='{$serviceId}' AND `{$this->paslaugu_kainos_lentele}`.`galioja_nuo`='{$validFrom}'";
		$data = mysql::select($query);
		
		return $data[0]['kiekis'];
	}

	public function getCustomerContracts($dateFrom, $dateTo) {
		$whereClauseString = "";
		if(!empty($dateFrom)) {
			$whereClauseString .= " WHERE `{$this->sutartys_lentele}`.`sutarties_data`>='{$dateFrom}'";
			if(!empty($dateTo)) {
				$whereClauseString .= " AND `{$this->sutartys_lentele}`.`sutarties_data`<='{$dateTo}'";
			}
		} else {
			if(!empty($dateTo)) {
				$whereClauseString .= " WHERE `{$this->sutartys_lentele}`.`sutarties_data`<='{$dateTo}'";
			}
		}
		
		$query = "  SELECT  `{$this->sutartys_lentele}`.`nr`,
							`{$this->sutartys_lentele}`.`sutarties_data`,
							`{$this->klientai_lentele}`.`asmens_kodas`,
							`{$this->klientai_lentele}`.`vardas`,
						    `{$this->klientai_lentele}`.`pavarde`,
						    `{$this->sutartys_lentele}`.`kaina` as `sutarties_kaina`,
						    IFNULL(sum(`{$this->uzsakytos_paslaugos_lentele}`.`kiekis` * `{$this->uzsakytos_paslaugos_lentele}`.`kaina`), 0) as `sutarties_paslaugu_kaina`,
						    `t`.`bendra_kliento_sutarciu_kaina`,
						    `s`.`bendra_kliento_paslaugu_kaina`
					FROM `{$this->sutartys_lentele}`
						INNER JOIN `{$this->klientai_lentele}`
							ON `{$this->sutartys_lentele}`.`fk_KLIENTASasmens_kodas`=`{$this->klientai_lentele}`.`asmens_kodas`
						LEFT JOIN `{$this->uzsakytos_paslaugos_lentele}`
							ON `{$this->sutartys_lentele}`.`nr`=`{$this->uzsakytos_paslaugos_lentele}`.`fk_SUTARTISnr`
						LEFT JOIN (
							SELECT `asmens_kodas`,
									sum(`{$this->sutartys_lentele}`.`kaina`) AS `bendra_kliento_sutarciu_kaina`
							FROM `{$this->sutartys_lentele}`
								INNER JOIN `{$this->klientai_lentele}`
									ON `{$this->sutartys_lentele}`.`fk_KLIENTASasmens_kodas`=`{$this->klientai_lentele}`.`asmens_kodas`
							{$whereClauseString}
							GROUP BY `asmens_kodas`
						) `t` ON `t`.`asmens_kodas`=`{$this->klientai_lentele}`.`asmens_kodas`
						LEFT JOIN (
							SELECT `asmens_kodas`,
									IFNULL(sum(`{$this->uzsakytos_paslaugos_lentele}`.`kiekis` * `{$this->uzsakytos_paslaugos_lentele}`.`kaina`), 0) as `bendra_kliento_paslaugu_kaina`
							FROM `{$this->sutartys_lentele}`
								INNER JOIN `{$this->klientai_lentele}`
									ON `{$this->sutartys_lentele}`.`fk_KLIENTASasmens_kodas`=`{$this->klientai_lentele}`.`asmens_kodas`
								LEFT JOIN `{$this->uzsakytos_paslaugos_lentele}`
									ON `{$this->sutartys_lentele}`.`nr`=`{$this->uzsakytos_paslaugos_lentele}`.`fk_SUTARTISnr`
								{$whereClauseString}							
								GROUP BY `asmens_kodas`
						) `s` ON `s`.`asmens_kodas`=`{$this->klientai_lentele}`.`asmens_kodas`
					{$whereClauseString}
					GROUP BY `{$this->sutartys_lentele}`.`nr` ORDER BY `{$this->klientai_lentele}`.`pavarde` ASC";
		$data = mysql::select($query);

		return $data;
	}
	
	public function getSumPriceOfContracts($dateFrom, $dateTo) {
		$whereClauseString = "";
		if(!empty($dateFrom)) {
			$whereClauseString .= " WHERE `{$this->sutartys_lentele}`.`sutarties_data`>='{$dateFrom}'";
			if(!empty($dateTo)) {
				$whereClauseString .= " AND `{$this->sutartys_lentele}`.`sutarties_data`<='{$dateTo}'";
			}
		} else {
			if(!empty($dateTo)) {
				$whereClauseString .= " WHERE `{$this->sutartys_lentele}`.`sutarties_data`<='{$dateTo}'";
			}
		}
		
		$query = "  SELECT sum(`{$this->sutartys_lentele}`.`kaina`) AS `nuomos_suma`
					FROM `{$this->sutartys_lentele}`
					{$whereClauseString}";
		$data = mysql::select($query);

		return $data;
	}

	public function getSumPriceOfOrderedServices($dateFrom, $dateTo) {
		$whereClauseString = "";
		if(!empty($dateFrom)) {
			$whereClauseString .= " WHERE `{$this->sutartys_lentele}`.`sutarties_data`>='{$dateFrom}'";
			if(!empty($dateTo)) {
				$whereClauseString .= " AND `{$this->sutartys_lentele}`.`sutarties_data`<='{$dateTo}'";
			}
		} else {
			if(!empty($dateTo)) {
				$whereClauseString .= " WHERE `{$this->sutartys_lentele}`.`sutarties_data`<='{$dateTo}'";
			}
		}
		
		$query = "  SELECT sum(`{$this->uzsakytos_paslaugos_lentele}`.`kiekis` * `{$this->uzsakytos_paslaugos_lentele}`.`kaina`) AS `paslaugu_suma`
					FROM `{$this->sutartys_lentele}`
						INNER JOIN `{$this->uzsakytos_paslaugos_lentele}`
							ON `{$this->sutartys_lentele}`.`nr`=`{$this->uzsakytos_paslaugos_lentele}`.`fk_SUTARTISnr`
					{$whereClauseString}";
		$data = mysql::select($query);

		return $data;
	}
	
	public function getDelayedCars($dateFrom, $dateTo) {
		$whereClauseString = "";
		if(!empty($dateFrom)) {
			$whereClauseString .= " AND `{$this->sutartys_lentele}`.`sutarties_data`>='{$dateFrom}'";
			if(!empty($dateTo)) {
				$whereClauseString .= " AND `{$this->sutartys_lentele}`.`sutarties_data`<='{$dateTo}'";
			}
		} else {
			if(!empty($dateTo)) {
				$whereClauseString .= " AND `{$this->sutartys_lentele}`.`sutarties_data`<='{$dateTo}'";
			}
		}
		
		$query = "  SELECT `nr`,
						   `sutarties_data`,
						   `planuojama_grazinimo_data`,
						   IF(`faktine_grazinimo_data`='0000-00-00 00:00:00', 'negrąžinta', `faktine_grazinimo_data`) AS `grazinta`,
						   `{$this->klientai_lentele}`.`vardas`,
						   `{$this->klientai_lentele}`.`pavarde`
					FROM `{$this->sutartys_lentele}`
						INNER JOIN `{$this->klientai_lentele}`
							ON `{$this->sutartys_lentele}`.`fk_KLIENTASasmens_kodas`=`{$this->klientai_lentele}`.`asmens_kodas`
					WHERE (DATEDIFF(`faktine_grazinimo_data`, `planuojama_grazinimo_data`) >= 1 OR
						(`faktine_grazinimo_data` = '0000-00-00 00:00:00' AND DATEDIFF(NOW(), `planuojama_grazinimo_data') >= 1))
					{$whereClauseString}";
		$data = mysql::select($query);

		return $data;
	}
	
}